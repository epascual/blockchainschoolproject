# Liga Larry La Langosta
Proyecto Final - Fundamentos de Blockchains - UNAM-IIMAS

## Integrantes
Victor Zamora Gutierrez

Eduardo Pascual Aseff

Zoe Leyva Acosta

Luis Felipe Benítez Lluis

## Idea
Sistema descentralizado que maneje intercambios de jugadores y selecciones de draft entre equipos de la liga Larry La Langosta


# Guia de instalación
1. Primero
En la misma carpeta donde se encuentra fabric-samples, extraer el repositorio en una carpeta. Forma recomendable, hacer **git clone git@gitlab.com:epascual/blockchainschoolproject.git** fuera de la carpeta fabric-samples.

2. Segundo
Si es la primera vez que se ejecuta el proyecto, copiar la carpeta test-network-extended a fabric-samples	

3. Tercero
Ejecutar ./mountSmartContracts.sh
Que hace este script:
- Copia los contratos en una carpeta LLL dentro de fabric-samples. (Esto no es lo más eficiente en cuanto a espacio, así que probablemente cambie en futuras veriones)
- Luego los compila.
- Luego copia la aplicación cliente
- Finalmente echa a andar la red.
Nota: En ocasiones falla en generar el bloque genesis o crear la red. Repetir este paso hasta que no de error.

4. Cuarto
Ejecutar ./installContracts.sh
Que hace este script:
- Empaca los smart contracts, y luego los instala en la red.
Nota: En ocasiones falla por timeout, porque la compilación con gradle demora, en ese caso de fallar, volver a ejecutar el script. Al parecer con la actualización a fabric2.1 este paso casi nunca falla más de una vez.

5. Quinto
Ejecutar ./approveSmartContracts.sh
Que hace este script:
- Aprueba en nombre de las organizaciones los Smart Contracts, y luego les hace commit.

Con estos 5 pasos, ya el chaincode debe estar ejecutándose sobre la red correctamente.

# Aplicaciones clientes
La aplicación cliente *LLLManager* fue desarrollada en Node.js (versión 8), por lo que este último es una dependencia necesaria para utilizarla.
Las aplicaciones clientes se encuentran dentro la carpeta LLL de fabric-samples. Por ejemplo, en fabric-samples/LLL/organizations/CrustaceoCascarudo/application se encuentra la aplicación para Crustaceo Cascarudo.
Desde el directorio de la aplicación, el cliente debe ser instalado con `npm install` una sola vez, para descargar las dependencias. 

El núcleo de la aplicación se encuentra distribuida en varios programas de Javascript, donde cada uno realiza una consulta o realiza una transacción a favor del equipo para la cual está configurada. Pero además, se cuenta con un programa gráfico que encapsula todas las funcionalidades.

OJO: Antes de utilizarla por primera vez, debe ejecutarse `node addToWallet.js`, para que se configuren los ficheros necesarios para la identificación del usuario en la red LLL Network. Aunque esto no es necesario si se utiliza la interfaz gráfica, donde se realiza de forma autómatica.

## Interfaz de línea de comandos
A continuación se listan los diferentes programas de línea de comandos con que cuenta la aplicación *LLLManager*.

### Consultas
Todas las consultas imprimen en pantalla el resultado en formato JSON. En los casos que el resultado son más de un asset, el resultado es un arreglo.

**queryPlayer.js** - Para consultar los datos de un jugador

Argumentos:
1. CURM del jugador a consultar

**queryDraft.js** - Para consultar los datos de un draft

Argumentos:
1. Año del draft
2. Número de Ronda
3. Equipo original al que pertenece

**queryMultiplePlayers** - Para consultar un grupo de jugadores. En el primer argumento se define que jugadores se están consultando: `ALL` para todos los jugadores de la liga, `TEAM` para los jugadores de un equipo y `FREE` para los jugadores cuyos contratos están vencidos.

Argumentos:
1. Tipo de Query (`ALL`, `TEAM` o `FREE`)
2. Equipo (Solo aplica cuando el primer argumento es `TEAM`)

**queryMultipleDrafts** - Para consultar un conjunto de drafts. En el primer argumento se define cuáles drafts se están consultando: `OBTAINED` para todos los drafts adquiridos por algún equipo, `TRADED` para los drafts que un equipo ha cedido y `ALL` para consultar todos los drafts intercambiados.

Argumentos:
1. Tipo de Query (`OBTAINED`, `TRADED` o `ALL`)
2. Equipo (No aplica cuando el primer argumento es `ALL`)

### Transacciones
Todas las transacciones imprimen en pantalla una cadena en formato JSON de la forma `{result:boolean, message:"..."}`. El valor de `result` es `true` o `false` en dependencia de si fue o no satisfactoria la transacción, y el valor de `message` es una descripción del resultado de la transacción.

**hirePlayer.js** - Programa para contratar a un jugador

Argumentos: 
1. CURM
2. Nombre del jugador
3. Equipo que contrata
4. Contrato que se establece con el jugador
5. Año hasta el cual será contratado

Ejemplo de uso:

    ? node hirePlayer.js CURM001 "Patricio" "Balde de Carnada" "150K" 2022
    {result:true, message:"Nuevo jugador con CURM CURM001 contratado exitosamente"}

**firePlayer.js** - Programa para terminar el contrato con un jugador

Argumentos: 
1. CURM
2. Equipo dueño del jugador

Ejemplo de uso:

    ? node firePlayer.js CURM004 "Balde de Carnada"
    {result:false, message:"El jugador con CURM CURM004 no esta actualmente contratado por su equipo"}

**exchange.js** - Programa para realizar un intercambio de drafts y/o jugadores con otro equipo

Argumentos: 
1. Lista de jugadores a intercambiar por el equipo1 (quien inicia la transacción)
2. Lista de drafts a intercambiar por el equipo1
3. Lista de jugadores a intercambiar por el equipo2
4. Lista de drafts a intercambiar por el equipo2
5. Nombre del equipo1
6. Nombre del equipo2

Nota: Los jugadores se especifican por un arreglo de CURMs, y los drafts por un arreglo de tres elementos. Por ejemplo `[CURM001, CURM002]`, `[año,ronda,equipo_original]`.

Ejemplo de uso:

    ? node exchange.js "[CURM001]" "[['2020','1','Balde de Carnada']]" \
                    "[CURM004]" "[['2021','2','Crustaceo Cascarudo']]" \
                    "Balde de Carnada" "Crustaceo Cascarudo"
    {result:true, message:"Intercambio realizado exitosamente"}

## Interfaz gráfica
Desde el directorio de la aplicación, debe ejecutar el comando `npm start` para iniciar la aplicación gráfica. Fue diseñada para permitir realizar todas consultas y transacciones relacionadas anteriormente, por medio de una interfaz amigable para el usuario, y en cualquier plataforma. 