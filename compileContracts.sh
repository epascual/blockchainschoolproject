#!/bin/bash

#Script para compilar los SmartContracts, los cuales son proyectos gradle de java.

pushd ../fabric-samples/LLL/organizations/BaldeCarnada/playercontract

#Asegurando que se pueda invocar gradlew
if [ ! -x ./gradlew ]
then
    chmod u+x gradlew
fi
./gradlew build

popd

pushd ../fabric-samples/LLL/organizations/CrustaceoCascarudo/playercontract

#Haciendo ejecutable gradlew
if [ ! -x ./gradlew ]
then
    chmod u+x gradlew
fi
./gradlew build

popd

pushd ../fabric-samples/LLL/organizations/OsosMarinos/playercontract

#Haciendo ejecutable gradlew
if [ ! -x ./gradlew ]
then
    chmod u+x gradlew
fi
./gradlew build

popd
