const TEAMNAME = "Balde de Carnada";

const teams = {
    BALDE_CARNADA : "Balde de Carnada",
    CRUSTACEO_CASCARUDO : "Crustaceo Cascarudo",
    OSOS_MARINOS: "Osos Marinos"
}

const walletDir = '../identity/user/balaji/wallet';
const userName = 'balaji';
const connectionProfile = '../gateway/connection-baldecarnada.yaml';

module.exports.TEAM = TEAMNAME;
module.exports.teams = teams;
module.exports.walletDir = walletDir;
module.exports.userName = userName;
module.exports.connectionProfile = connectionProfile;
