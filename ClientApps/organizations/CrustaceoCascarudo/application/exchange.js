/*
SPDX-License-Identifier: Apache-2.0
*/

/*
 * Aplicación para hacer las transacciones de assets entre los dos equipos.
 */

'use strict';

// Bring key classes into scope, most importantly Fabric SDK network class
const fs = require('fs');
const yaml = require('js-yaml');
const { Wallets, Gateway } = require('fabric-network');
const Constants = require('./lib/custom');

let response;

let logText = "";
function log(text){
    logText += text + '\n';
}

// Main program function
async function main() {

    // A wallet stores a collection of identities for use
    const wallet = await Wallets.newFileSystemWallet(Constants.walletDir);

    // A gateway defines the peers used to access Fabric networks
    const gateway = new Gateway();

    // Main try/catch block
    try {

        // Specify userName for network access
        const userName = Constants.userName;

        // Load connection profile; will be used to locate a gateway
        let connectionProfile = yaml.safeLoad(fs.readFileSync(Constants.connectionProfile, 'utf8'));

        // Set connection options; identity and wallet
        let connectionOptions = {
            identity: userName,
            wallet: wallet,
            discovery: { enabled:true, asLocalhost: true }
        };

        // Connect to gateway using application specified parameters
        log('Connect to Fabric gateway.');

        await gateway.connect(connectionProfile, connectionOptions);

        // Access PaperNet network
        log('Use network channel: mychannel.');

        const network = await gateway.getNetwork('mychannel');

        // Get addressability to commercial paper contract
        log('Use org.papernet.commercialpaper smart contract.');

        const contract = await network.getContract('playercontract');

        log('Submit Trade Transaction.');

        let jugadores1 = process.argv[2];
        let jugadores2 = process.argv[3];
        let drafts1 = process.argv[4];
        let drafts2 = process.argv[5];
        let equipo1 = process.argv[6];
        let equipo2 = process.argv[7];
        /*
        let jugadores1 = "[]";
        let drafts1 = "[['2020','1','Balde de Carnada']]";
        let jugadores2 = "[]";
        let drafts2 = "[['2020','1','Crustaceo Cascarudo']]";
        let equipo1 = "Balde de Carnada";
        let equipo2 = "Crustaceo Cascarudo";
        //let year = 2020;
        */

        const issueResponse = await contract.submitTransaction(
            "trade", jugadores1, drafts1, jugadores2, drafts2, equipo1, equipo2
        );

        // process response
        log('Process issue transaction response.'+issueResponse);
        response = JSON.parse(issueResponse.toString())
        response = JSON.stringify(response);

        log('Transaction complete.');

    } catch (error) {

        log(`Error processing transaction. ${error}`);
        log(error.stack);
        console.error(logText);
        process.exit(1);

    } finally {

        // Disconnect from the gateway
        log('Disconnect from Fabric gateway.');
        gateway.disconnect();

    }
}
main().then(() => {

    log('Issue program complete.');
    console.log(response);

}).catch((e) => {

    log('Issue program exception.');
    log(e);
    log(e.stack);

    console.error(logText);
    process.exit(-1);

});
