/*
SPDX-License-Identifier: Apache-2.0
*/

'use strict';

// Bring key classes into scope, most importantly Fabric SDK network class
const fs = require('fs');
const yaml = require('js-yaml');
const { Wallets, Gateway } = require('fabric-network');
const Draft = require('./lib/draft');
const Constants = require('./lib/custom');

let draftsResponse;

let logText = "";
function log(text){
    logText += text + '\n';
}

// Main program function
async function main() {
    // A wallet stores a collection of identities for use
    const wallet = await Wallets.newFileSystemWallet(Constants.walletDir);

    // A gateway defines the peers used to access Fabric networks
    const gateway = new Gateway();

    // Main try/catch block
    try {

        // Specify userName for network access
        const userName = Constants.userName;

        // Load connection profile; will be used to locate a gateway
        let connectionProfile = yaml.safeLoad(fs.readFileSync(Constants.connectionProfile, 'utf8'));

        // Set connection options; identity and wallet
        let connectionOptions = {
            identity: userName,
            wallet: wallet,
            discovery: { enabled:true, asLocalhost: true }
        };

        // Connect to gateway using application specified parameters
        log('Connect to Fabric gateway.');

        await gateway.connect(connectionProfile, connectionOptions);

        // Access PaperNet network
        log('Use network channel: mychannel.');

        const network = await gateway.getNetwork('mychannel');

        // Get addressability to player contract
        log('Use player smart contract.');

        const contract = await network.getContract('playercontract');

        let option = process.argv[2];
        let team;
        let response;

        switch (option){
        case "OBTAINED":
            // query obtainded draft of a team
            team = process.argv[3];
            log('Submit queryObtainedDrafts Transaction.');
            response = await contract.evaluateTransaction(
                "queryObtainedDrafts", team
            );
            log(`Transaction has been evaluated, result is: ` + response);

            draftsResponse = Draft.fromArrayBuffer(response);
            draftsResponse = JSON.stringify(draftsResponse);
            break;
        case "TRADED":
            // query traded draft of a team
            team = process.argv[3];
            log('Submit queryTradedDrafts Transaction.');
            response = await contract.evaluateTransaction(
                "queryTradedDrafts", team
            );
            log(`Transaction has been evaluated, result is: ` + response);

            draftsResponse = Draft.fromArrayBuffer(response);
            draftsResponse = JSON.stringify(draftsResponse);
            break;
        case "ALL":
            // query traded draft of a team
            log('Submit queryAllDrafts Transaction.');
            response = await contract.evaluateTransaction(
                "queryAllDrafts"
            );
            log(`Transaction has been evaluated, result is: ` + response);

            draftsResponse = Draft.fromArrayBuffer(response);
            draftsResponse = JSON.stringify(draftsResponse);
            break;
        default:
            draftsResponse = JSON.stringify("[]");
            break;
        }

    } catch (error) {

        log(`Error processing transaction. ${error}`);
        log(error.stack);
        console.error(logText);
        process.exit(-1);

    } finally {

        // Disconnect from the gateway
        log('Disconnect from Fabric gateway.');
        gateway.disconnect();

    }
}
main().then(() => {

    console.log(draftsResponse);
    log('Issue program complete.');

}).catch((e) => {

    log('Issue program exception.');
    log(e);
    log(e.stack);
    console.error(logText);
    process.exit(-1);
    
});
