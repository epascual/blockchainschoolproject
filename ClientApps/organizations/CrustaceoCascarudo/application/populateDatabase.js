/*
SPDX-License-Identifier: Apache-2.0
*/

/*
 * El objetivo de este fichero es insertar datos de jugadores en el
 * ledger de forma tal que pueda ser posible probar las funcionalidades.
 */

'use strict';

// Bring key classes into scope, most importantly Fabric SDK network class
const fs = require('fs');
const yaml = require('js-yaml');
const { Wallets, Gateway } = require('fabric-network');
const Player = require('./lib/player');
const Constants = require('./lib/custom');

let playerReturned;

let logText = "";
function log(text){
    logText += text + '\n';
}

let playersToPopulate = [
    Player.createInstance("CURM001","Patricio",Constants.teams.BALDE_CARNADA,"150K","2018","2022"),
    Player.createInstance("CURM002","Gary",Constants.teams.BALDE_CARNADA,"10K","2017","2021"),
    Player.createInstance("CURM003","Don Cangrejo",Constants.teams.BALDE_CARNADA,"1M","2019","2022"),
    Player.createInstance("CURM004","Calamardo",Constants.teams.CRUSTACEO_CASCARUDO,"300K","2019","2022"),
    Player.createInstance("CURM005","Arenita",Constants.teams.CRUSTACEO_CASCARUDO,"250K","2018","2021"),
    Player.createInstance("CURM006","Plankton",Constants.teams.CRUSTACEO_CASCARUDO,"1.1M","2017","2023")
];

// Main program function
async function main() {

    // A wallet stores a collection of identities for use
    const wallet = await Wallets.newFileSystemWallet(Constants.walletDir);

    // A gateway defines the peers used to access Fabric networks
    const gateway = new Gateway();

    // Main try/catch block
    try {

        // Specify userName for network access
        const userName = Constants.userName;

        // Load connection profile; will be used to locate a gateway
        let connectionProfile = yaml.safeLoad(fs.readFileSync(Constants.connectionProfile, 'utf8'));

        // Set connection options; identity and wallet
        let connectionOptions = {
            identity: userName,
            wallet: wallet,
            discovery: { enabled:true, asLocalhost: true }
        };

        // Connect to gateway using application specified parameters
        log('Connect to Fabric gateway.');

        await gateway.connect(connectionProfile, connectionOptions);

        // Access PaperNet network
        log('Use network channel: mychannel.');

        const network = await gateway.getNetwork('mychannel');

        // Get addressability to player contract
        log('Use player smart contract.');

        const contract = await network.getContract('playercontract');

        log('Empezar a popular BD.');

        for (const pop of playersToPopulate ){            
            let curm = pop.curm;
            let playerName = pop.name;
            let team = pop.owner;
            let playerContract = pop.contract;
            let finalYear = pop.lastyearhired;

            log('Insertando jugador con CURM: ' + curm);

            const issueResponse = await contract.submitTransaction(
                "hirePlayer", curm, playerName, team, playerContract, finalYear
            );

            if (!issueResponse){
                console.log("Error insertando jugador con CURM: " + curm);
            }

            // process response
            log('Process issue transaction response.'+issueResponse);
        };

        log('Transaction complete.');

    } catch (error) {

        log(`Error processing transaction. ${error}`);
        log(error.stack);

    } finally {

        // Disconnect from the gateway
        log('Disconnect from Fabric gateway.');
        gateway.disconnect();

    }
}
main().then(() => {

    

    log('Issue program complete.');
    console.log("Population completed");

}).catch((e) => {
    log('Issue program exception.');
    log(e);
    log(e.stack);

    console.error(logText);

    process.exit(-1);

});
/*
for (const pop of playersToPopulate ){
    //console.log(pop);

    let curm = pop.curm;
    let playerName = pop.name;
    let team = pop.owner;
    let playerContract = pop.contract;
    let initialYear = pop.yearhired;
    let finalYear = pop.lastyearhired;

    console.log(curm);
    console.log(playerName);
    console.log(team);
    console.log(playerContract);
    console.log(initialYear);
    console.log(finalYear);
}
*/