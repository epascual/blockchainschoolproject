/*
SPDX-License-Identifier: Apache-2.0
*/

'use strict';

// Utility class for ledger state
const State = require('./../ledger-api/state.js');

/**
 * Draft class extends State class
 * Class will be used by application
 */
class Draft extends State {

    constructor(obj) {
        super(Draft.getClass(), [obj.year, obj.round, obj.originalTeam]);
        Object.assign(this, obj);
    }

    /**
     * Basic getters and setters
    */
    
    getYear(){
        return this.year;
    }
    setYear(newYear){
        this.year = newYear;
    }

    getRound(){
        return this.round;
    }
    setRound(newRound){
        this.round = newRound;
    }

    getOriginalTeam(){
        return this.originalTeam;
    }
    setOriginalTeam(newOriginalTeam){
        this.originalTeam = newOriginalTeam;
    }

    getOwnerTeam(){
        return this.ownerTeam;
    }
    setOwnerTeam(newOwnerTeam){
        this.ownerTeam = newOwnerTeam;
    }
    
    
    static fromBuffer(buffer) {
        return Draft.deserialize(buffer);
    }

    static fromArrayBuffer(buffer){
        return Draft.deserializeArray(buffer);
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this));
    }

    /**
     * Deserialize a state data to commercial paper
     * @param {Buffer} data to form back into the object
     */
    static deserialize(data) {
        return State.deserializeClass(data, Draft);
    }

    static deserializeArray(data) {
        let json = JSON.parse(data.toString());
        return json;
    }

    /**
     * Factory method to create a commercial paper object
     */
    static createInstance(year, round, originalTeam, ownerTeam) {
        return new Draft({ year, round, originalTeam, ownerTeam });
    }

    static getClass() {
        return 'mx.unamiimas.blockchains.draft';
    }
}

module.exports = Draft;
