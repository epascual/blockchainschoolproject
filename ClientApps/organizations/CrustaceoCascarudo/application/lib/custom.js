const TEAMNAME = "Crustaceo Cascarudo";

const teams = {
    BALDE_CARNADA : "Balde de Carnada",
    CRUSTACEO_CASCARUDO : "Crustaceo Cascarudo"
}

const walletDir = '../identity/user/isabella/wallet';
const userName = 'isabella';
const connectionProfile = '../gateway/connection-crustaceocascarudo.yaml';

module.exports.TEAM = TEAMNAME;
module.exports.teams = teams;
module.exports.walletDir = walletDir;
module.exports.userName = userName;
module.exports.connectionProfile = connectionProfile;