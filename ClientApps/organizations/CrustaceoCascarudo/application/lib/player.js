/*
SPDX-License-Identifier: Apache-2.0
*/

'use strict';

// Utility class for ledger state
const State = require('./../ledger-api/state.js');

/**
 * Player class extends State class
 * Class will be used by application
 */
class Player extends State {

    constructor(obj) {
        super(Player.getClass(), [obj.curm]);
        Object.assign(this, obj);
    }

    /**
     * Basic getters and setters
    */
    
    ///CLAVE UNICA DE REGISTRO MARITIMO
    getCurm(){
        return this.curm;
    }
    setCurm(newCurm){
        this.curm = newCurm;
    }

    getName(){
        return this.name;
    }
    setName(newName){
        this.name = newName;
    }

    getOwner(){
        return this.owner;
    }
    setOwner(newOwner){
        this.owner = newOwner;
    }

    getContract(){
        return this.contract;
    }
    setContract(newContract){
        this.contract = newContract;
    }

    getYearHired(){
        return this.yearHired;
    }
    setYearHired(newYearHired){
        this.yearHired = newYearHired;
    }

    getLastYearHired(){
        return this.lastyearhired;
    }
    setLastYearHired(newLastyearhired){
        this.lastyearhired = newLastyearhired;
    }


    static fromBuffer(buffer) {
        return Player.deserialize(buffer);
    }

    static fromArrayBuffer(buffer){
        return Player.deserializeArray(buffer);
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this));
    }

    /**
     * Deserialize a state data to commercial paper
     * @param {Buffer} data to form back into the object
     */
    static deserialize(data) {
        return State.deserializeClass(data, Player);
    }

    static deserializeArray(data) {
        let json = JSON.parse(data.toString());
        return json;
    }

    /**
     * Factory method to create a commercial paper object
     */
    static createInstance(curm, name, owner, contract, yearhired, lastyearhired) {
        return new Player({ curm, name, owner, contract, yearhired, lastyearhired });
    }

    static getClass() {
        return 'mx.unamiimas.blockchains.player';
    }
}

module.exports = Player;
