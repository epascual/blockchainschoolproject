/*
SPDX-License-Identifier: Apache-2.0
*/

'use strict';

// Bring key classes into scope, most importantly Fabric SDK network class
const fs = require('fs');
const yaml = require('js-yaml');
const { Wallets, Gateway } = require('fabric-network');
const Draft = require('./lib/draft');
const Constants = require('./lib/custom');

let draft;

let logText = "";
function log(text){
    logText += text + '\n';
}

// Main program function
async function main() {
    // A wallet stores a collection of identities for use
    const wallet = await Wallets.newFileSystemWallet(Constants.walletDir);

    // A gateway defines the peers used to access Fabric networks
    const gateway = new Gateway();

    // Main try/catch block
    try {

        // Specify userName for network access
        const userName = Constants.userName;

        // Load connection profile; will be used to locate a gateway
        let connectionProfile = yaml.safeLoad(fs.readFileSync(Constants.connectionProfile, 'utf8'));

        // Set connection options; identity and wallet
        let connectionOptions = {
            identity: userName,
            wallet: wallet,
            discovery: { enabled:true, asLocalhost: true }
        };

        // Connect to gateway using application specified parameters
        log('Connect to Fabric gateway.');

        await gateway.connect(connectionProfile, connectionOptions);

        // Access PaperNet network
        log('Use network channel: mychannel.');

        const network = await gateway.getNetwork('mychannel');

        // Get addressability to player contract
        log('Use playercontract smart contract.');

        const contract = await network.getContract('playercontract');

        // query draft
        log('Submit Query Draft Transaction.');

        let year = process.argv[2];
        let round = process.argv[3];
        let originalTeam = process.argv[4];

        const issueResponse = await contract.evaluateTransaction(
            "queryDraft", year, round, originalTeam
        );

        draft = Draft.fromBuffer(issueResponse);

        log(`Transaction has been evaluated, result is: `+issueResponse);
        log(`QUERY RESULT OF ${draft.year} : ${draft.round}, ${draft.originalTeam}, ${draft.ownerTeam}`);

        //Para returnarlo a la UI
        draft = JSON.stringify(draft);

    } catch (error) {

        log(`Error processing transaction. ${error}`);
        log(error.stack);
        console.error(logText);
        process.exit(-1);

    } finally {

        // Disconnect from the gateway
        log('Disconnect from Fabric gateway.');
        gateway.disconnect();

    }
}
main().then(() => {

    console.log(draft);
    log('Issue program complete.');

}).catch((e) => {

    log('Issue program exception.');
    log(e);
    log(e.stack);
    console.error(logText);
    process.exit(-1);
    
});
