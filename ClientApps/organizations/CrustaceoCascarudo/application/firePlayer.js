/*
SPDX-License-Identifier: Apache-2.0
*/

/*
 * Aplicación para finalizar contrato con un jugador
 */

'use strict';

// Bring key classes into scope, most importantly Fabric SDK network class
const fs = require('fs');
const yaml = require('js-yaml');
const { Wallets, Gateway } = require('fabric-network');
const Player = require('./lib/player');
const Constants = require('./lib/custom');

let playerReturned;

let logText = "";
function log(text){
    logText += text + '\n';
}


// Main program function
async function main() {

    // A wallet stores a collection of identities for use
    const wallet = await Wallets.newFileSystemWallet(Constants.walletDir);

    // A gateway defines the peers used to access Fabric networks
    const gateway = new Gateway();

    // Main try/catch block
    try {

        // Specify userName for network access
        const userName = Constants.userName;

        // Load connection profile; will be used to locate a gateway
        let connectionProfile = yaml.safeLoad(fs.readFileSync(Constants.connectionProfile, 'utf8'));

        // Set connection options; identity and wallet
        let connectionOptions = {
            identity: userName,
            wallet: wallet,
            discovery: { enabled:true, asLocalhost: true }
        };

        // Connect to gateway using application specified parameters
        log('Connect to Fabric gateway.');

        await gateway.connect(connectionProfile, connectionOptions);

        // Access PaperNet network
        log('Use network channel: mychannel.');

        const network = await gateway.getNetwork('mychannel');

        // Get addressability to commercial paper contract
        log('Use player smart contract.');

        const contract = await network.getContract('playercontract');

        // issue commercial paper
        log('Submit Player Hire Transaction.');

        let curm = process.argv[2];
        let owner = process.argv[3];

        const issueResponse = await contract.submitTransaction(
            "firePlayer", curm, owner
        );

        // process response
        log('Process issue transaction response.'+issueResponse);

        let player = JSON.parse(issueResponse.toString())
        log('Player is: ' + player);
        playerReturned = JSON.stringify(player);

        log('Transaction complete.');

    } catch (error) {

        log(`Error processing transaction. ${error}`);
        log(error.stack);
        console.error(logText);
        process.exit(-1);

    } finally {

        // Disconnect from the gateway
        log('Disconnect from Fabric gateway.');
        gateway.disconnect();

    }
}
main().then(() => {

    log('Issue program complete.');
    console.log(playerReturned);

}).catch((e) => {

    log('Issue program exception.');
    log(e);
    log(e.stack);

    console.error(logText);
    process.exit(-1);

});
