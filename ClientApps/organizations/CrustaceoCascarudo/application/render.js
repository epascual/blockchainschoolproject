const { spawn } = require('child_process');

const hirePlayerBtn = document.getElementById('hirePlayer');
const firePlayerBtn = document.getElementById('firePlayer');

const queryPlayerBtn = document.getElementById('queryPlayer');
const queryDraftBtn = document.getElementById('queryDraft');
const queryAllPlayersBtn = document.getElementById('queryAllPlayers');
const queryTeemPlayersBtn = document.getElementById('queryTeemPlayers');
const queryFreePlayersBtn = document.getElementById('queryFreePlayers');
const queryTradedDraftsBtn = document.getElementById('queryTradedDrafts');

const tradeAssetsBtn = document.getElementById('tradeAssets');


//BOB esponja curm ESBO950508RFC00
//const Player = require('./lib/player');
const Constants = require('./lib/custom');
const THIS_TEAM = Constants.TEAM;
const THIS_YEAR = 2020;

const playerToRow = function(player){
    let playerAsArray = [player.curm, player.name, player.owner, player.yearhired+"-"+player.lastyearhired, player.contract];
    let row = "<tr><td>" + playerAsArray.join("</td><td>") + "</td></tr>";
    return row;
};
const draftToRow = function(draft){
    let draftAsArray = [draft.originalTeam, draft.year, draft.round, draft.ownerTeam];
    let row = "<tr><td>" + draftAsArray.join("</td><td>") + "</td></tr>";
    return row;
};

const setMessage = function(element, message, success){
    let elementDiv = document.getElementById(element);
    let text;
    if (success){
        text = "<div class=\"alert alert-success alert-dismissible\">" + 
                "<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"+
                message + "</div>";
    } else {
        text = "<div class=\"alert alert-danger alert-dismissible\">" + 
                "<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>" +
                message + "</div>";
    }
    elementDiv.innerHTML = text;
}

hirePlayerBtn.onclick = function(){
    //let tmp = spawn('node', ['./hireNewPlayer.js','ESBO950508RFC00','Bob Esponja',
    //                         Constants.teams.BALDE_CARNADA,'50M','2019','2021']);
    let params = [
        './hirePlayer.js',
        hireCurm.value,
        hireName.value,
        THIS_TEAM,
        hireContract.value,
        hireFinalYear.value,
        THIS_YEAR
    ];
    let tmp = spawn('node', params);

    //console.log(params);

    let updateResult;
    let updateMessage;
    let error="";

    tmp.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
        let parsed = JSON.parse(data);
        updateResult = parsed.result;
        updateMessage = parsed.message;
    });
    tmp.stderr.on('data', (data) => {
        //console.error(`stderr: ${data}`);
        error += data;
    });
    tmp.on('close', (code) => {
        if (code === 0){
            setMessage('hirePlayerMessage', updateMessage, updateResult);
        } else {
            setMessage('hirePlayerMessage', "Error: " + error, false);
        }
    });
}

firePlayerBtn.onclick = function(){
    let params = [
        './firePlayer.js',
        fireCurm.value,
        THIS_TEAM,
        THIS_YEAR
    ]
    let tmp = spawn('node', params );

    let error = "";
    let updateResult;
    let updateMessage;

    tmp.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
        let parsed = JSON.parse(data);
        updateResult = parsed.result;
        updateMessage = parsed.message;
    });
    tmp.stderr.on('data', (data) => {
        //console.error(`stderr: ${data}`);
        error += data;
    });
    tmp.on('close', (code) => {
        if (code === 0){
            setMessage('firePlayerMessage', updateMessage, updateResult);
        } else {
            setMessage('firePlayerMessage', "Error: " + error, false);
        }
    });
}

/********
 * Ocultar/Desocultar entradas para los drafts a intercambiar
*/
const updateElementVisibility = function(state, element){
    if (state){
        element.style.display = "block";
    } else {
        element.style.display = "none";
    }
}
document.getElementById('tradeDraftOwn1').onchange = function(){
    updateElementVisibility(this.checked, document.getElementById('inputDraft1Div'));
}
document.getElementById('tradeDraftOwn2').onchange = function(){
    updateElementVisibility(this.checked, document.getElementById('inputDraft2Div'));    
}
document.getElementById('tradeDraftOwn3').onchange = function(){
    updateElementVisibility(this.checked, document.getElementById('inputDraft3Div'));
}
document.getElementById('tradeDraftOther1').onchange = function(){
    updateElementVisibility(this.checked, document.getElementById('inputDraft1otherDiv'));
}
document.getElementById('tradeDraftOther2').onchange = function(){
    updateElementVisibility(this.checked, document.getElementById('inputDraft2otherDiv'));
}
document.getElementById('tradeDraftOther3').onchange = function(){
    updateElementVisibility(this.checked, document.getElementById('inputDraft3otherDiv'));
}

const stringArrayToString = function(arr){
    let ret;
    if (arr.length > 0 && !(arr.length==1 && arr[0]=="")){
        ret = "['" + arr.join("','") + "']";
    } else {
        ret = "[]";
    }
    return ret;
}
const arrayToString = function(arr){
    let ret;
    if (arr.length > 0 && !(arr.length==1 && arr[0]=="")){
        ret = "[" + arr.join(",") + "]";
    } else {
        ret = "[]";
    }
    return ret;
}
tradeAssetsBtn.onclick = function(){
    
    let ownPlayersArray = ownPlayersExchange.value.split(' ');
    let otherPlayersArray = otherPlayersExchange.value.split(' ');

    let ownDraftsArray = [];
    let otherDraftsArray = [];

    if (document.getElementById('tradeDraftOwn1').checked){
        ownDraftsArray.push("['" + 
            ownDraftsExchangeYear1.value + "','" +
            ownDraftsExchangeRound1.value + "','" +
            ownDraftsExchangeOwner1.value + "']"
        );
    }
    if (document.getElementById('tradeDraftOwn2').checked){
        ownDraftsArray.push("['" + 
            ownDraftsExchangeYear2.value + "','" +
            ownDraftsExchangeRound2.value + "','" +
            ownDraftsExchangeOwner2.value + "']"
        );
    }
    if (document.getElementById('tradeDraftOwn3').checked){
        ownDraftsArray.push("['" + 
            ownDraftsExchangeYear3.value + "','" +
            ownDraftsExchangeRound3.value + "','" +
            ownDraftsExchangeOwner3.value + "']"
        );
    }

    if (document.getElementById('tradeDraftOther1').checked){
        otherDraftsArray.push("['" + 
            otherDraftsExchangeYear1.value + "','" +
            otherDraftsExchangeRound1.value + "','" +
            otherDraftsExchangeOwner1.value + "']"
        );
    }
    if (document.getElementById('tradeDraftOther2').checked){
        otherDraftsArray.push("['" + 
            otherDraftsExchangeYear2.value + "','" +
            otherDraftsExchangeRound2.value + "','" +
            otherDraftsExchangeOwner2.value + "']"
        );
    }
    if (document.getElementById('tradeDraftOther3').checked){
        otherDraftsArray.push("['" + 
            otherDraftsExchangeYear3.value + "','" +
            otherDraftsExchangeRound3.value + "','" +
            otherDraftsExchangeOwner3.value + "']"
        );
    }

    ownPlayersArray = stringArrayToString(ownPlayersArray);
    otherPlayersArray = stringArrayToString(otherPlayersArray);
    ownDraftsArray = arrayToString(ownDraftsArray);
    otherDraftsArray = arrayToString(otherDraftsArray);

    let params = [
        'exchange.js',
        ownPlayersArray,
        otherPlayersArray,
        ownDraftsArray,
        otherDraftsArray,
        THIS_TEAM,
        exchangerTeam.value,
        THIS_YEAR
    ];
    
    let tmp = spawn('node', params);

    let updateResult;
    let updateMessage;
    let error = "";

    tmp.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
        let parsed = JSON.parse(data);
        updateResult = parsed.result;
        updateMessage = parsed.message;
    });
    tmp.stderr.on('data', (data) => {
        //console.error(`stderr: ${data}`);
        error += data;
    });
    tmp.on('close', (code) => {
        if (code === 0){
            setMessage('tradeMessage', updateMessage, updateResult);
        } else {
            setMessage('tradeMessage', "Error: " + error, false);
        }
    });
}

/***********************************************
 * Seccion para Queries.                       *
 *                                             *
 ***********************************************/
queryPlayerBtn.onclick = function(){
    let tmp = spawn('node', ['./queryPlayer.js', queryCurm.value]);

    let error="";
    let player = "";

    tmp.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
        player = JSON.parse(data);
    });
    tmp.stderr.on('data', (data) => {
        //console.error(`stderr: ${data}`);
        error += data;
    });
    tmp.on('close', (code) => {
        let table = document.getElementById("PlayerTableBody");
        table.innerHTML = "";
        if (code === 0){
            table.innerHTML = playerToRow(player);
        } else {
            //message.innerHTML = "Error consultando jugador";
            console.log(`QueryPlayer process exited with code ${code}`);
        }
    });
}

queryDraftBtn.onclick = function(){
    let params = [
        './queryDraft.js', 
        queryDraftYear.value,
        queryDraftRound.value,
        queryDraftOrigTeam.value
    ];
    let tmp = spawn('node', params);
    let error = "";
    let draft = "";

    tmp.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
        draft = JSON.parse(data);
    });
    tmp.stderr.on('data', (data) => {
        //console.error(`stderr: ${data}`);
        error += data;
    });
    tmp.on('close', (code) => {
        let table = document.getElementById("DraftsPicksTableBody");
        table.innerHTML = "";

        if (code === 0){
            table.innerHTML = draftToRow(draft);
        } else {
            //message.innerHTML = "Error consultando draft";
            console.log(`QueryDraft process exited with code ${code}`);
        }
    });
}

queryAllPlayersBtn.onclick = function(){
    let tmp = spawn('node', ['./queryMultiplePlayers.js', 'ALL']);

    let error = "";
    let players = "";

    tmp.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
        players = JSON.parse(data);
    });
    tmp.stderr.on('data', (data) => {
        //console.error(`stderr: ${data}`);
        error += data;
    });
    tmp.on('close', (code) => {
        let table = document.getElementById("AllPlayersTableBody");
        table.innerHTML = "";
        
        if (code === 0){
            let rows = "";
            for (var i=0; i < players.length; i++){
                player = players[i];    
                rows += playerToRow(player);
            }
            table.innerHTML = rows;
        } else {
            //message.innerHTML = "Error en consulta";
            console.log(`QueryPlayer process exited with code ${code}`);
            console.log(error);
        }
    });
}

queryTeemPlayersBtn.onclick = function(){
    let params = ['./queryMultiplePlayers.js', 'TEAM', queryTeamPlayersTeam.value];
    let tmp = spawn('node', params);

    let error = "";
    let players = "";

    tmp.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
        players = JSON.parse(data);
    });
    tmp.stderr.on('data', (data) => {
        //console.error(`stderr: ${data}`);
        error += data;
    });
    tmp.on('close', (code) => {
        let table = document.getElementById("TeamPlayersTableBody");
        table.innerHTML = "";
        
        if (code === 0){
            let rows = "";
            for (var i=0; i < players.length; i++){
                player = players[i];    
                rows += playerToRow(player);
            }
            table.innerHTML = rows;
        } else {
            //message.innerHTML = "Error en consulta";
            console.log(`QueryPlayer process exited with code ${code}`);
            console.log(error);
        }
    });
}

queryFreePlayersBtn.onclick = function(){
    let tmp = spawn('node', ['./queryMultiplePlayers.js', 'FREE']);

    let error = "";
    let players = "";

    tmp.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
        players = JSON.parse(data);
    });
    tmp.stderr.on('data', (data) => {
        //console.error(`stderr: ${data}`);
        error += data;
    });
    tmp.on('close', (code) => {
        let table = document.getElementById("FreePlayersTableBody");
        table.innerHTML = "";
        
        if (code === 0){
            let rows = "";
            for (var i=0; i < players.length; i++){
                player = players[i];    
                rows += playerToRow(player);
            }
            table.innerHTML = rows;
        } else {
            //message.innerHTML = "Error en consulta";
            console.log(`QueryPlayer process exited with code ${code}`);
            console.log(error);
        }
    });
}

const radioTradedDraftsChanged = function(){
    if (document.getElementById('radioTradedDrafts3').checked){
        document.getElementById('queryTradedDraftTeamDiv').style.display = "none";
    } else {
        document.getElementById('queryTradedDraftTeamDiv').style.display = "block";
    }
}

document.getElementById('radioTradedDrafts1').onchange = radioTradedDraftsChanged;
document.getElementById('radioTradedDrafts2').onchange = radioTradedDraftsChanged;
document.getElementById('radioTradedDrafts3').onchange = radioTradedDraftsChanged;

queryTradedDraftsBtn.onclick = function(){
    let params = ['./queryMultipleDrafts.js', '', queryTradedDraftTeam.value];

    let radios = document.getElementsByName('radioTradedDrafts');
    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            params[1] = radios[i].value;
            break;
        }
    }

    let tmp = spawn('node', params);

    let error = "";
    let drafts = "";

    tmp.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
        drafts = JSON.parse(data);
    });
    tmp.stderr.on('data', (data) => {
        //console.error(`stderr: ${data}`);
        error += data;
    });
    tmp.on('close', (code) => {
        let table = document.getElementById("TradedDraftTableBody");
        table.innerHTML = "";
        
        if (code === 0){
            let rows = "";
            for (var i=0; i < drafts.length; i++){
                let draft = drafts[i];    
                rows += draftToRow(draft);
            }
            table.innerHTML = rows;
        } else {
            //message.innerHTML = "Error en consulta";
            console.log(`QueryTradedDrafts process exited with code ${code}`);
            console.log(error);
        }
    });
}
/***********************************************
 * Fin de Seccion para Queries.                *
 ***********************************************/


var addToWalletFn = function(){
    let tmp = spawn('node', ['./addToWallet.js']);

    tmp.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
    });
    tmp.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
    });
    tmp.on('close', (code) => {
        if (code === 0){
            //message.innerHTML = "Agregado al wallet";
            console.log("Agregado al Wallet");
        } else {
            console.log(`AddToWallet exited with code ${code}`);
        }
    });
};
addToWalletFn();

/***********************************************
 * Solo para desarrollo.                       *
 **********************************************
const populateBtn = document.getElementById('populate');
populateBtn.onclick = function(){
    let tmp = spawn('node', ['./populateDatabase.js']);

    let msg = "";

    tmp.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
        msg += data;
    });
    tmp.stderr.on('data', (data) => {
        console.error(`stderr: ${data}`);
    });
    tmp.on('close', (code) => {
        if (code === 0){
            //TODO CHIDO :)
        } else {
            console.log(`PopulateDB process exited with code ${code}`);
        }
    });
}
*/