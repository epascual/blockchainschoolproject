const TEAMNAME = "Osos Marinos";

const teams = {
    BALDE_CARNADA : "Balde de Carnada",
    CRUSTACEO_CASCARUDO : "Crustaceo Cascarudo",
    OSOS_MARINOS: "Osos Marinos"
}

const walletDir = '../identity/user/osito/wallet';
const userName = 'osito';
const connectionProfile = '../gateway/connection-ososmarinos.yaml';

module.exports.TEAM = TEAMNAME;
module.exports.teams = teams;
module.exports.walletDir = walletDir;
module.exports.userName = userName;
module.exports.connectionProfile = connectionProfile;
