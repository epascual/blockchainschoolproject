#!/bin/bash

echo "Copiando la aplicación a Crustaceo Cascarudo"
cp -R ./ClientApps/organizations/CrustaceoCascarudo/application \
     ../fabric-samples/LLL/organizations/CrustaceoCascarudo

cp ./ClientApps/organizations/CrustaceoAddToWallet.js \
   ../fabric-samples/LLL/organizations/CrustaceoCascarudo/application/addToWallet.js

cp ./ClientApps/organizations/CrustaceoCustom.js \
   ../fabric-samples/LLL/organizations/CrustaceoCascarudo/application/lib/custom.js

#echo "NO SE ESTA COPIANDO CLIENTE BALDE CARNADA PARA GANAR EN TIEMPO"
#exit 0 

echo "Copiando la aplicación a Balde Carnada"
cp -R ./ClientApps/organizations/CrustaceoCascarudo/application \
     ../fabric-samples/LLL/organizations/BaldeCarnada

cp ./ClientApps/organizations/BaldeAddToWallet.js \
   ../fabric-samples/LLL/organizations/BaldeCarnada/application/addToWallet.js

cp ./ClientApps/organizations/BaldeCustom.js \
   ../fabric-samples/LLL/organizations/BaldeCarnada/application/lib/custom.js

cp ./ClientApps/organizations/CrustaceoCascarudo/application/img/Img4.jpg \
   ../fabric-samples/LLL/organizations/BaldeCarnada/application/img/Img1.jpg



echo "Copiando la aplicación a Osos Marinos"
cp -R ./ClientApps/organizations/CrustaceoCascarudo/application \
     ../fabric-samples/LLL/organizations/OsosMarinos

cp ./ClientApps/organizations/OsosAddToWallet.js \
   ../fabric-samples/LLL/organizations/OsosMarinos/application/addToWallet.js

cp ./ClientApps/organizations/OsosCustom.js \
   ../fabric-samples/LLL/organizations/OsosMarinos/application/lib/custom.js

cp ./ClientApps/organizations/CrustaceoCascarudo/application/img/Img5.jpg \
   ../fabric-samples/LLL/organizations/OsosMarinos/application/img/Img1.jpg