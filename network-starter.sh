#!/bin/bash
#
# SPDX-License-Identifier: Apache-2.0

function _exit(){
    printf "Exiting:%s\n" "$1"
    exit -1
}

# Exit on first error, print all commands.
set -ev
set -o pipefail

# Where am I?
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export FABRIC_CFG_PATH="${DIR}/../config"

cd "${DIR}/../test-network-extended/"

docker kill cliDigiBank cliMagnetoCorp logspout || true

./network.sh down
./network.sh up createChannel -ca -s couchdb

# Copy the connection profiles so they are in the correct organizations.
cp "${DIR}/../test-network-extended/organizations/peerOrganizations/baldecarnada.lll.mx/connection-baldecarnada.yaml" "${DIR}/organizations/BaldeCarnada/gateway/"
cp "${DIR}/../test-network-extended/organizations/peerOrganizations/crustaceocascarudo.lll.mx/connection-crustaceocascarudo.yaml" "${DIR}/organizations/CrustaceoCascarudo/gateway/"
cp "${DIR}/../test-network-extended/organizations/peerOrganizations/ososmarinos.lll.mx/connection-ososmarinos.yaml" "${DIR}/organizations/OsosMarinos/gateway/"

cp ${DIR}/../test-network-extended/organizations/peerOrganizations/baldecarnada.lll.mx/users/User1@baldecarnada.lll.mx/msp/signcerts/* ${DIR}/../test-network-extended/organizations/peerOrganizations/baldecarnada.lll.mx/users/User1@baldecarnada.lll.mx/msp/signcerts/User1@baldecarnada.lll.mx-cert.pem
cp ${DIR}/../test-network-extended/organizations/peerOrganizations/baldecarnada.lll.mx/users/User1@baldecarnada.lll.mx/msp/keystore/* ${DIR}/../test-network-extended/organizations/peerOrganizations/baldecarnada.lll.mx/users/User1@baldecarnada.lll.mx/msp/keystore/priv_sk

cp ${DIR}/../test-network-extended/organizations/peerOrganizations/crustaceocascarudo.lll.mx/users/User1@crustaceocascarudo.lll.mx/msp/signcerts/* ${DIR}/../test-network-extended/organizations/peerOrganizations/crustaceocascarudo.lll.mx/users/User1@crustaceocascarudo.lll.mx/msp/signcerts/User1@crustaceocascarudo.lll.mx-cert.pem
cp ${DIR}/../test-network-extended/organizations/peerOrganizations/crustaceocascarudo.lll.mx/users/User1@crustaceocascarudo.lll.mx/msp/keystore/* ${DIR}/../test-network-extended/organizations/peerOrganizations/crustaceocascarudo.lll.mx/users/User1@crustaceocascarudo.lll.mx/msp/keystore/priv_sk

cp ${DIR}/../test-network-extended/organizations/peerOrganizations/ososmarinos.lll.mx/users/User1@ososmarinos.lll.mx/msp/signcerts/* ${DIR}/../test-network-extended/organizations/peerOrganizations/ososmarinos.lll.mx/users/User1@ososmarinos.lll.mx/msp/signcerts/User1@ososmarinos.lll.mx-cert.pem
cp ${DIR}/../test-network-extended/organizations/peerOrganizations/ososmarinos.lll.mx/users/User1@ososmarinos.lll.mx/msp/keystore/* ${DIR}/../test-network-extended/organizations/peerOrganizations/ososmarinos.lll.mx/users/User1@ososmarinos.lll.mx/msp/keystore/priv_sk

echo Suggest that you monitor the docker containers by running
echo "./organization/magnetocorp/configuration/cli/monitordocker.sh net_test"
