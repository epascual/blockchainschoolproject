#Script para poner a correr la red, y luego instalar el chaincode

TEAM1="BaldeCarnada"
TEAM2="CrustaceoCascarudo"
TEAM3="OsosMarinos"

#Dirigiendose a la carpeta LLL
cd ../fabric-samples/LLL/

function installChaincode {
  echo "Montando contrato para $1"
  pushd ./organizations/$1/

  echo "Poniendo variables de entorno"
  source $1.sh

  export PACKAGE_ID=$(peer lifecycle chaincode queryinstalled --output json | jq -r '.installed_chaincodes[0].package_id')
  
  echo "Instalado con PACKAGE_ID: $PACKAGE_ID"
  echo ""
  echo "Aprovando para organizacion de $1"

  peer lifecycle chaincode approveformyorg  --orderer localhost:7050 --ordererTLSHostnameOverride orderer.example.com \
                                          --channelID mychannel  \
                                          --name playercontract  \
                                          -v 0  \
                                          --package-id $PACKAGE_ID \
                                          --sequence 1  \
                                          --tls  \
                                          --cafile $ORDERER_CA

  echo ""
  echo "Haciendo chaincode checkcommitreadiness"
  peer lifecycle chaincode checkcommitreadiness --channelID mychannel --name playercontract -v 0 --sequence 1
  echo "LISTO PARA $1"
  echo ""

  popd
}

installChaincode $TEAM1
installChaincode $TEAM2
installChaincode $TEAM3

echo ""
echo "Committing Smart Contract"
peer lifecycle chaincode commit -o localhost:7050 \
                                --peerAddresses localhost:7051 --tlsRootCertFiles ${PEER0_ORG1_CA} \
                                --peerAddresses localhost:9051 --tlsRootCertFiles ${PEER0_ORG2_CA} \
                                --peerAddresses localhost:10051 --tlsRootCertFiles ${PEER0_ORG3_CA} \
                                --ordererTLSHostnameOverride orderer.example.com \
                                --channelID mychannel --name playercontract -v 0 \
                                --sequence 1 \
                                --tls --cafile $ORDERER_CA --waitForEvent 

