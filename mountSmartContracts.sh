#!/bin/bash

# El repositorio blockchainschoolproject debe estar en el mismo directorio que se encuentra fabric-samples.
# Este script se debe ejectuar desde donde se encuentra: la carpeta blockchainsschoolproject
# El objetivo del script es copiar para fabric-samples el smart contract, junto a la configuración necesaria para instalarlo


#Variables para los equipos
TEAM1="BaldeCarnada"
TEAM2="CrustaceoCascarudo"
TEAM3="OsosMarinos"

#Primero, si existe, borrar la versión de LLL de la carpeta fabric-samples
if [ -d "../fabric-samples/LLL" ] ; then
	echo "Borrando carpeta LLL"
	rm -R "../fabric-samples/LLL/"
fi

#Luego, crear la carpeta para empezar a copiar:

echo "Creando directorios LLL"
mkdir ../fabric-samples/LLL

echo "Copiando los Smart Contracts y otros scripts"
cp -R ./LLL/organizations/ ../fabric-samples/LLL/

#COPIANDO LOS FICHEROS DE GRADLE QUE FUNCIONAN
rm ../fabric-samples/LLL/organizations/BaldeCarnada/playercontract/gradlew*
rm ../fabric-samples/LLL/organizations/BaldeCarnada/playercontract/*.gradle
cp GradleOriginal/* ../fabric-samples/LLL/organizations/BaldeCarnada/playercontract

#CREANDO CARPETA GATEWAY
mkdir ../fabric-samples/LLL/organizations/BaldeCarnada/gateway

#COPIANDO FICHEROS DE INICIO-PARO DE LA RED
cp ./network-clean.sh ../fabric-samples/LLL/
cp ./network-starter.sh ../fabric-samples/LLL/

echo "Duplicando Smart Contracts a nombre de otros equipos-organizaciones"
cp -R ../fabric-samples/LLL/organizations/BaldeCarnada ../fabric-samples/LLL/organizations/CrustaceoCascarudo
cp -R ../fabric-samples/LLL/organizations/BaldeCarnada ../fabric-samples/LLL/organizations/OsosMarinos

echo "Copiando ficheros para establecer identidad en el entorno"
cp Enviroments/BaldeCarnada.sh ../fabric-samples/LLL/organizations/BaldeCarnada/
cp Enviroments/CrustaceoCascarudo.sh ../fabric-samples/LLL/organizations/CrustaceoCascarudo/
cp Enviroments/OsosMarinos.sh ../fabric-samples/LLL/organizations/OsosMarinos/

echo "Compilando los smart contracts"
./compileContracts.sh

echo "Copiando aplicaciones cliente"
./deployClients.sh
echo "Fin de copia"

echo "Iniciando la red! (./network-starter.sh)"
#Dirigiendose a la carpeta LLL
pushd ../fabric-samples/LLL/
./network-starter.sh
popd

echo "Fin de mountSmartContracts.sh"


