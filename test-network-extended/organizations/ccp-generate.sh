#!/bin/bash

function one_line_pem {
    echo "`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $1`"
}

function json_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s/\${oRG}/$6/" \
        -e "s/\${ORGFULL}/$7/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        organizations/ccp-template.json
}

function yaml_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
		-e "s/\${oRG}/$6/" \
        -e "s/\${ORGFULL}/$7/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        organizations/ccp-template.yaml | sed -e $'s/\\\\n/\\\n        /g'
}

ORG=BaldeCarnada
oRG=baldecarnada
ORGFULL=baldecarnada.lll.mx
P0PORT=7051
CAPORT=7054
PEERPEM=organizations/peerOrganizations/baldecarnada.lll.mx/tlsca/tlsca.baldecarnada.lll.mx-cert.pem
CAPEM=organizations/peerOrganizations/baldecarnada.lll.mx/ca/ca.baldecarnada.lll.mx-cert.pem

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $oRG $ORGFULL)" > organizations/peerOrganizations/baldecarnada.lll.mx/connection-baldecarnada.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $oRG $ORGFULL)" > organizations/peerOrganizations/baldecarnada.lll.mx/connection-baldecarnada.yaml

ORG=CrustaceoCascarudo
oRG=crustaceocascarudo
ORGFULL=crustaceocascarudo.lll.mx
P0PORT=9051
CAPORT=8054
PEERPEM=organizations/peerOrganizations/crustaceocascarudo.lll.mx/tlsca/tlsca.crustaceocascarudo.lll.mx-cert.pem
CAPEM=organizations/peerOrganizations/crustaceocascarudo.lll.mx/ca/ca.crustaceocascarudo.lll.mx-cert.pem

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $oRG $ORGFULL)" > organizations/peerOrganizations/crustaceocascarudo.lll.mx/connection-crustaceocascarudo.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $oRG $ORGFULL)" > organizations/peerOrganizations/crustaceocascarudo.lll.mx/connection-crustaceocascarudo.yaml

ORG=OsosMarinos
oRG=ososmarinos
ORGFULL=ososmarinos.lll.mx
P0PORT=10051
CAPORT=10054
PEERPEM=organizations/peerOrganizations/ososmarinos.lll.mx/tlsca/tlsca.ososmarinos.lll.mx-cert.pem
CAPEM=organizations/peerOrganizations/ososmarinos.lll.mx/ca/ca.ososmarinos.lll.mx-cert.pem

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $oRG $ORGFULL)" > organizations/peerOrganizations/ososmarinos.lll.mx/connection-ososmarinos.json
echo "$(yaml_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $oRG $ORGFULL)" > organizations/peerOrganizations/ososmarinos.lll.mx/connection-ososmarinos.yaml
    