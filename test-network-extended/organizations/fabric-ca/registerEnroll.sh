

function createBaldeCarnada {

  echo
	echo "Enroll the CA admin"
  echo
	mkdir -p organizations/peerOrganizations/baldecarnada.lll.mx/

	export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/
#  rm -rf $FABRIC_CA_CLIENT_HOME/fabric-ca-client-config.yaml
#  rm -rf $FABRIC_CA_CLIENT_HOME/msp

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:7054 --caname ca-baldecarnada --tls.certfiles ${PWD}/organizations/fabric-ca/baldecarnada/tls-cert.pem
  set +x

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-baldecarnada.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-baldecarnada.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-baldecarnada.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-baldecarnada.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/msp/config.yaml

  echo
	echo "Register peer0"
  echo
  set -x
	fabric-ca-client register --caname ca-baldecarnada --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/baldecarnada/tls-cert.pem
  set +x

  echo
  echo "Register user"
  echo
  set -x
  fabric-ca-client register --caname ca-baldecarnada --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/baldecarnada/tls-cert.pem
  set +x

  echo
  echo "Register the org admin"
  echo
  set -x
  fabric-ca-client register --caname ca-baldecarnada --id.name baldecarnadaadmin --id.secret baldecarnadaadminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/baldecarnada/tls-cert.pem
  set +x

	mkdir -p organizations/peerOrganizations/baldecarnada.lll.mx/peers
  mkdir -p organizations/peerOrganizations/baldecarnada.lll.mx/peers/peer0.baldecarnada.lll.mx

  echo
  echo "## Generate the peer0 msp"
  echo
  set -x
	fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7054 --caname ca-baldecarnada -M ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/peers/peer0.baldecarnada.lll.mx/msp --csr.hosts peer0.baldecarnada.lll.mx --tls.certfiles ${PWD}/organizations/fabric-ca/baldecarnada/tls-cert.pem
  set +x

  cp ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/msp/config.yaml ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/peers/peer0.baldecarnada.lll.mx/msp/config.yaml

  echo
  echo "## Generate the peer0-tls certificates"
  echo
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7054 --caname ca-baldecarnada -M ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/peers/peer0.baldecarnada.lll.mx/tls --enrollment.profile tls --csr.hosts peer0.baldecarnada.lll.mx --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/baldecarnada/tls-cert.pem
  set +x


  cp ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/peers/peer0.baldecarnada.lll.mx/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/peers/peer0.baldecarnada.lll.mx/tls/ca.crt
  cp ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/peers/peer0.baldecarnada.lll.mx/tls/signcerts/* ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/peers/peer0.baldecarnada.lll.mx/tls/server.crt
  cp ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/peers/peer0.baldecarnada.lll.mx/tls/keystore/* ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/peers/peer0.baldecarnada.lll.mx/tls/server.key

  mkdir ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/msp/tlscacerts
  cp ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/peers/peer0.baldecarnada.lll.mx/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/msp/tlscacerts/ca.crt

  mkdir ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/tlsca
  cp ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/peers/peer0.baldecarnada.lll.mx/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/tlsca/tlsca.baldecarnada.lll.mx-cert.pem

  mkdir ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/ca
  cp ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/peers/peer0.baldecarnada.lll.mx/msp/cacerts/* ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/ca/ca.baldecarnada.lll.mx-cert.pem

  mkdir -p organizations/peerOrganizations/baldecarnada.lll.mx/users
  mkdir -p organizations/peerOrganizations/baldecarnada.lll.mx/users/User1@baldecarnada.lll.mx

  echo
  echo "## Generate the user msp"
  echo
  set -x
	fabric-ca-client enroll -u https://user1:user1pw@localhost:7054 --caname ca-baldecarnada -M ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/users/User1@baldecarnada.lll.mx/msp --tls.certfiles ${PWD}/organizations/fabric-ca/baldecarnada/tls-cert.pem
  set +x

  mkdir -p organizations/peerOrganizations/baldecarnada.lll.mx/users/Admin@baldecarnada.lll.mx

  echo
  echo "## Generate the org admin msp"
  echo
  set -x
	fabric-ca-client enroll -u https://baldecarnadaadmin:baldecarnadaadminpw@localhost:7054 --caname ca-baldecarnada -M ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/users/Admin@baldecarnada.lll.mx/msp --tls.certfiles ${PWD}/organizations/fabric-ca/baldecarnada/tls-cert.pem
  set +x

  cp ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/msp/config.yaml ${PWD}/organizations/peerOrganizations/baldecarnada.lll.mx/users/Admin@baldecarnada.lll.mx/msp/config.yaml

}


function createCrustaceoCascarudo {

  echo
	echo "Enroll the CA admin"
  echo
	mkdir -p organizations/peerOrganizations/crustaceocascarudo.lll.mx/

	export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/
#  rm -rf $FABRIC_CA_CLIENT_HOME/fabric-ca-client-config.yaml
#  rm -rf $FABRIC_CA_CLIENT_HOME/msp

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:8054 --caname ca-crustaceocascarudo --tls.certfiles ${PWD}/organizations/fabric-ca/crustaceocascarudo/tls-cert.pem
  set +x

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-crustaceocascarudo.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-crustaceocascarudo.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-crustaceocascarudo.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-crustaceocascarudo.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/msp/config.yaml

  echo
	echo "Register peer0"
  echo
  set -x
	fabric-ca-client register --caname ca-crustaceocascarudo --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/crustaceocascarudo/tls-cert.pem
  set +x

  echo
  echo "Register user"
  echo
  set -x
  fabric-ca-client register --caname ca-crustaceocascarudo --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/crustaceocascarudo/tls-cert.pem
  set +x

  echo
  echo "Register the org admin"
  echo
  set -x
  fabric-ca-client register --caname ca-crustaceocascarudo --id.name crustaceocascarudoadmin --id.secret crustaceocascarudoadminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/crustaceocascarudo/tls-cert.pem
  set +x

	mkdir -p organizations/peerOrganizations/crustaceocascarudo.lll.mx/peers
  mkdir -p organizations/peerOrganizations/crustaceocascarudo.lll.mx/peers/peer0.crustaceocascarudo.lll.mx

  echo
  echo "## Generate the peer0 msp"
  echo
  set -x
	fabric-ca-client enroll -u https://peer0:peer0pw@localhost:8054 --caname ca-crustaceocascarudo -M ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/peers/peer0.crustaceocascarudo.lll.mx/msp --csr.hosts peer0.crustaceocascarudo.lll.mx --tls.certfiles ${PWD}/organizations/fabric-ca/crustaceocascarudo/tls-cert.pem
  set +x

  cp ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/msp/config.yaml ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/peers/peer0.crustaceocascarudo.lll.mx/msp/config.yaml

  echo
  echo "## Generate the peer0-tls certificates"
  echo
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:8054 --caname ca-crustaceocascarudo -M ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/peers/peer0.crustaceocascarudo.lll.mx/tls --enrollment.profile tls --csr.hosts peer0.crustaceocascarudo.lll.mx --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/crustaceocascarudo/tls-cert.pem
  set +x


  cp ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/peers/peer0.crustaceocascarudo.lll.mx/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/peers/peer0.crustaceocascarudo.lll.mx/tls/ca.crt
  cp ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/peers/peer0.crustaceocascarudo.lll.mx/tls/signcerts/* ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/peers/peer0.crustaceocascarudo.lll.mx/tls/server.crt
  cp ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/peers/peer0.crustaceocascarudo.lll.mx/tls/keystore/* ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/peers/peer0.crustaceocascarudo.lll.mx/tls/server.key

  mkdir ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/msp/tlscacerts
  cp ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/peers/peer0.crustaceocascarudo.lll.mx/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/msp/tlscacerts/ca.crt

  mkdir ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/tlsca
  cp ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/peers/peer0.crustaceocascarudo.lll.mx/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/tlsca/tlsca.crustaceocascarudo.lll.mx-cert.pem

  mkdir ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/ca
  cp ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/peers/peer0.crustaceocascarudo.lll.mx/msp/cacerts/* ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/ca/ca.crustaceocascarudo.lll.mx-cert.pem

  mkdir -p organizations/peerOrganizations/crustaceocascarudo.lll.mx/users
  mkdir -p organizations/peerOrganizations/crustaceocascarudo.lll.mx/users/User1@crustaceocascarudo.lll.mx

  echo
  echo "## Generate the user msp"
  echo
  set -x
	fabric-ca-client enroll -u https://user1:user1pw@localhost:8054 --caname ca-crustaceocascarudo -M ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/users/User1@crustaceocascarudo.lll.mx/msp --tls.certfiles ${PWD}/organizations/fabric-ca/crustaceocascarudo/tls-cert.pem
  set +x

  mkdir -p organizations/peerOrganizations/crustaceocascarudo.lll.mx/users/Admin@crustaceocascarudo.lll.mx

  echo
  echo "## Generate the org admin msp"
  echo
  set -x
	fabric-ca-client enroll -u https://crustaceocascarudoadmin:crustaceocascarudoadminpw@localhost:8054 --caname ca-crustaceocascarudo -M ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/users/Admin@crustaceocascarudo.lll.mx/msp --tls.certfiles ${PWD}/organizations/fabric-ca/crustaceocascarudo/tls-cert.pem
  set +x

  cp ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/msp/config.yaml ${PWD}/organizations/peerOrganizations/crustaceocascarudo.lll.mx/users/Admin@crustaceocascarudo.lll.mx/msp/config.yaml

}

function createOrderer {

  echo
	echo "Enroll the CA admin"
  echo
	mkdir -p organizations/ordererOrganizations/example.com

	export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/ordererOrganizations/example.com
#  rm -rf $FABRIC_CA_CLIENT_HOME/fabric-ca-client-config.yaml
#  rm -rf $FABRIC_CA_CLIENT_HOME/msp

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:9054 --caname ca-orderer --tls.certfiles ${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem
  set +x

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-orderer.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-orderer.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-orderer.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-orderer.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/ordererOrganizations/example.com/msp/config.yaml


  echo
	echo "Register orderer"
  echo
  set -x
	fabric-ca-client register --caname ca-orderer --id.name orderer --id.secret ordererpw --id.type orderer --tls.certfiles ${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem
    set +x

  echo
  echo "Register the orderer admin"
  echo
  set -x
  fabric-ca-client register --caname ca-orderer --id.name ordererAdmin --id.secret ordererAdminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem
  set +x

	mkdir -p organizations/ordererOrganizations/example.com/orderers
  mkdir -p organizations/ordererOrganizations/example.com/orderers/example.com

  mkdir -p organizations/ordererOrganizations/example.com/orderers/orderer.example.com

  echo
  echo "## Generate the orderer msp"
  echo
  set -x
	fabric-ca-client enroll -u https://orderer:ordererpw@localhost:9054 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp --csr.hosts orderer.example.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem
  set +x

  cp ${PWD}/organizations/ordererOrganizations/example.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/config.yaml

  echo
  echo "## Generate the orderer-tls certificates"
  echo
  set -x
  fabric-ca-client enroll -u https://orderer:ordererpw@localhost:9054 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/tls --enrollment.profile tls --csr.hosts orderer.example.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem
  set +x

  cp ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/tls/ca.crt
  cp ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/tls/signcerts/* ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/tls/server.crt
  cp ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/tls/keystore/* ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/tls/server.key

  mkdir ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

  mkdir ${PWD}/organizations/ordererOrganizations/example.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/example.com/msp/tlscacerts/tlsca.example.com-cert.pem

  mkdir -p organizations/ordererOrganizations/example.com/users
  mkdir -p organizations/ordererOrganizations/example.com/users/Admin@example.com

  echo
  echo "## Generate the admin msp"
  echo
  set -x
	fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@localhost:9054 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/example.com/users/Admin@example.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem
  set +x

  cp ${PWD}/organizations/ordererOrganizations/example.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/example.com/users/Admin@example.com/msp/config.yaml


}



function createOsosMarinos {

  echo
	echo "Enroll the CA admin"
  echo
	mkdir -p organizations/peerOrganizations/ososmarinos.lll.mx/

	export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/
#  rm -rf $FABRIC_CA_CLIENT_HOME/fabric-ca-client-config.yaml
#  rm -rf $FABRIC_CA_CLIENT_HOME/msp

  set -x
  fabric-ca-client enroll -u https://admin:adminpw@localhost:10054 --caname ca-ososmarinos --tls.certfiles ${PWD}/organizations/fabric-ca/ososmarinos/tls-cert.pem
  set +x

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-10054-ca-ososmarinos.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-10054-ca-ososmarinos.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-10054-ca-ososmarinos.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-10054-ca-ososmarinos.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/msp/config.yaml

  echo
	echo "Register peer0"
  echo
  set -x
	fabric-ca-client register --caname ca-ososmarinos --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/ososmarinos/tls-cert.pem
  set +x

  echo
  echo "Register user"
  echo
  set -x
  fabric-ca-client register --caname ca-ososmarinos --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/ososmarinos/tls-cert.pem
  set +x

  echo
  echo "Register the org admin"
  echo
  set -x
  fabric-ca-client register --caname ca-ososmarinos --id.name ososmarinosadmin --id.secret ososmarinosadminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/ososmarinos/tls-cert.pem
  set +x

	mkdir -p organizations/peerOrganizations/ososmarinos.lll.mx/peers
  mkdir -p organizations/peerOrganizations/ososmarinos.lll.mx/peers/peer0.ososmarinos.lll.mx

  echo
  echo "## Generate the peer0 msp"
  echo
  set -x
	fabric-ca-client enroll -u https://peer0:peer0pw@localhost:10054 --caname ca-ososmarinos -M ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/peers/peer0.ososmarinos.lll.mx/msp --csr.hosts peer0.ososmarinos.lll.mx --tls.certfiles ${PWD}/organizations/fabric-ca/ososmarinos/tls-cert.pem
  set +x

  cp ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/msp/config.yaml ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/peers/peer0.ososmarinos.lll.mx/msp/config.yaml

  echo
  echo "## Generate the peer0-tls certificates"
  echo
  set -x
  fabric-ca-client enroll -u https://peer0:peer0pw@localhost:10054 --caname ca-ososmarinos -M ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/peers/peer0.ososmarinos.lll.mx/tls --enrollment.profile tls --csr.hosts peer0.ososmarinos.lll.mx --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/ososmarinos/tls-cert.pem
  set +x


  cp ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/peers/peer0.ososmarinos.lll.mx/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/peers/peer0.ososmarinos.lll.mx/tls/ca.crt
  cp ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/peers/peer0.ososmarinos.lll.mx/tls/signcerts/* ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/peers/peer0.ososmarinos.lll.mx/tls/server.crt
  cp ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/peers/peer0.ososmarinos.lll.mx/tls/keystore/* ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/peers/peer0.ososmarinos.lll.mx/tls/server.key

  mkdir ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/msp/tlscacerts
  cp ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/peers/peer0.ososmarinos.lll.mx/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/msp/tlscacerts/ca.crt

  mkdir ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/tlsca
  cp ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/peers/peer0.ososmarinos.lll.mx/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/tlsca/tlsca.ososmarinos.lll.mx-cert.pem

  mkdir ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/ca
  cp ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/peers/peer0.ososmarinos.lll.mx/msp/cacerts/* ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/ca/ca.ososmarinos.lll.mx-cert.pem

  mkdir -p organizations/peerOrganizations/ososmarinos.lll.mx/users
  mkdir -p organizations/peerOrganizations/ososmarinos.lll.mx/users/User1@ososmarinos.lll.mx

  echo
  echo "## Generate the user msp"
  echo
  set -x
	fabric-ca-client enroll -u https://user1:user1pw@localhost:10054 --caname ca-ososmarinos -M ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/users/User1@ososmarinos.lll.mx/msp --tls.certfiles ${PWD}/organizations/fabric-ca/ososmarinos/tls-cert.pem
  set +x

  mkdir -p organizations/peerOrganizations/ososmarinos.lll.mx/users/Admin@ososmarinos.lll.mx

  echo
  echo "## Generate the org admin msp"
  echo
  set -x
	fabric-ca-client enroll -u https://ososmarinosadmin:ososmarinosadminpw@localhost:10054 --caname ca-ososmarinos -M ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/users/Admin@ososmarinos.lll.mx/msp --tls.certfiles ${PWD}/organizations/fabric-ca/ososmarinos/tls-cert.pem
  set +x

  cp ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/msp/config.yaml ${PWD}/organizations/peerOrganizations/ososmarinos.lll.mx/users/Admin@ososmarinos.lll.mx/msp/config.yaml

}