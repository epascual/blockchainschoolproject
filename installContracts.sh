#Script para poner a correr la red, y luego instalar el chaincode

TEAM1="BaldeCarnada"
TEAM2="CrustaceoCascarudo"
TEAM3="OsosMarinos"

#Dirigiendose a la carpeta LLL
cd ../fabric-samples/LLL/

function installChaincode {
  echo ""
  echo "!!! Montando contrato para $1!!!"
  pushd ./organizations/$1/

  echo "Poniendo variables de entorno"
  source $1.sh

  echo ""
  echo "Empacando Smart contract"
  peer lifecycle chaincode package cp.tar.gz --lang java --path ./playercontract --label cp_0

  echo ""
  echo "Instalando chaincode"
  peer lifecycle chaincode install cp.tar.gz
  
  popd
}

installChaincode $TEAM1
installChaincode $TEAM2
installChaincode $TEAM3
