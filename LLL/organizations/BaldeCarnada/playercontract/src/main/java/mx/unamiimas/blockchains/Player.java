/*
 * SPDX-License-Identifier: Apache-2.0
 */

package mx.unamiimas.blockchains;

import static java.nio.charset.StandardCharsets.UTF_8;
import java.util.Objects;

import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;
import org.json.JSONObject;

import mx.unamiimas.blockchains.ledgerapi.State;

@DataType()
public class Player extends State {
	
	/* ATRIBUTOS */

    @Property()
    private String curm; ///CLAVE UNICA DE REGISTRO MARITIMO

    @Property()
    private String name;

    @Property()
    private String owner;

    @Property()
    private String contract;
    
    @Property()
    private int yearhired;
    
    @Property()
    private int lastyearhired;
    
    /* GETTERS AND SETTERS */

	public Player setCurm(String curm) {
		this.curm = curm;
		return this;
	}

	public String getCurm(){
		return curm;
	}

	public String getName() {
		return name;
	}

	public Player setName(String name) {
		this.name = name;
		return this;
	}

	public String getContract() {
		return contract;
	}

	public Player setContract(String contract) {
		this.contract = contract;
		return this;
	}

    public String getOwner() {
        return owner;
    }
    
    public Player setOwner(String owner) {
        this.owner = owner;
        return this;
    }
    
    public int getYearhired() {
	    return yearhired;
    }
    
    public Player setYearhired(int yearhired) {
        this.yearhired = yearhired;
        return this;
    }
	
    public int getLastyearhired() {
	    return lastyearhired;
    }
    
    public Player setLastyearhired(int lastyearhired) {
        this.lastyearhired = lastyearhired;
        return this;
    }

    /**
     * Nos dice si el jugador es agente libre.
     * @param year El anio actual
     * @return <code>true</code> si el jugador es agente libre y <code>false</code> en otro caso.
     */
    public boolean esAgenteLibre(int year){
	    return year > this.lastyearhired;
    } 
    
    /* CONSTRUCTOR */

    public Player() {
        super();
    }
    
    /* METODOS DE STATE: setKey, toString, deserialize, serialize, createInstance */
    
    public Player setKey() {
        this.key = State.makeKey(new String[] { this.curm });
        return this;
    }
    
    @Override
    public String toString() {
        return "Player::" + this.key + " " + this.getName() + " from " + 
        			this.getOwner() + " contract: " + this.getContract();
    }

    /**
     * Deserialize a state data to commercial paper
     *
     * @param {Buffer} data to form back into the object
     */
    public static Player deserialize(byte[] data) {
        JSONObject json;
		try{
        	json = new JSONObject(new String(data, UTF_8));
		} catch (org.json.JSONException exc){
			return null;
		}
        
        String curm = json.getString("curm");
        String name = json.getString("name");
        String owner = json.getString("owner");
        String contract = json.getString("contract");
        int year = json.getInt("yearhired");
        int lastyear = json.getInt("lastyearhired");
        
        return createInstance(curm, name, owner, contract, year, lastyear);
    }

    public static byte[] serialize(Player paper) {
        return State.serialize(paper);
    }

    /**
     * Factory method to create a commercial paper object
     */
    public static Player createInstance(String curm, String name, String owner,
            String contract, int yearhired, int lastyearhired) {
        return new Player().setCurm(curm).setName(name).setOwner(owner)
        		.setContract(contract).setYearhired(yearhired).setLastyearhired(lastyearhired).setKey();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        Player other = (Player) obj;

        return Objects.deepEquals(
            new String[] {getCurm(), getName(), getContract(), getOwner()},
            new String[] {other.getCurm(), other.getName(), other.getContract(), other.getOwner()}
        ) && getYearhired() == other.getYearhired() && getLastyearhired() == other.getLastyearhired();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCurm(), getName(), getContract(), getOwner(), getYearhired(), getLastyearhired());
    }
}
