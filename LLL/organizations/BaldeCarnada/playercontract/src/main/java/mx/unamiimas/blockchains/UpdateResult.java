package mx.unamiimas.blockchains;

import java.util.Objects;

import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;

@DataType()
public class UpdateResult {

    @Property()
    private final boolean result;

    @Property()
    private final String message;

    public UpdateResult(boolean result, String message){
        this.result = result;
        this.message = message;
    }

    public boolean getResult(){return result;}
    public String getMessage(){return message;}


    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        UpdateResult other = (UpdateResult) obj;

        return getResult() == other.getResult() && getMessage().contentEquals(other.getMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getResult(), this.getMessage());
    }

    @Override
    public String toString() {
        return "{\"Result\":\"" + result + "\"" + "\"Message\":{\"" + message + "}\"}";
    }
}