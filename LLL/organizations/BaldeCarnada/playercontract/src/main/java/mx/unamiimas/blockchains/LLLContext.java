package mx.unamiimas.blockchains;

import org.hyperledger.fabric.contract.Context;
import org.hyperledger.fabric.shim.ChaincodeStub;

class LLLContext extends Context {

    public LLLContext(ChaincodeStub stub) {
        super(stub);
        this.playerList = new PlayerList(this);
        this.draftList = new DraftList(this);
    }

    public PlayerList playerList;
    public DraftList draftList;

}