package mx.unamiimas.blockchains;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.util.Objects;

import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;
import org.json.JSONObject;
import org.json.JSONException;

import mx.unamiimas.blockchains.ledgerapi.State;

@DataType
public class Draft extends State {
	
	/* ATRIBUTOS */
	
	@Property()
	private int year;
	
	@Property()
	private int round;
	
	@Property()
	private String originalTeam;
	
	@Property()
	private String ownerTeam;
	
	/* GETTERS AND SETTERS */
	public int getYear() {
		return year;
	}

	public Draft setYear(int year) {
		this.year = year;
		return this;
	}

	public int getRound() {
		return round;
	}

	public Draft setRound(int round) {
		this.round = round;
		return this;
	}

	public String getOriginalTeam() {
		return originalTeam;
	}

	public Draft setOriginalTeam(String originalTeam) {
		this.originalTeam = originalTeam;
		return this;
	}

	public String getOwnerTeam() {
		return ownerTeam;
	}

	public Draft setOwnerTeam(String ownerTeam) {
		this.ownerTeam = ownerTeam;
		return this;
	}
	
	/* CONSTRUCTOR */
	public Draft() {
		super();
	}
	
	/* METODOS DE STATE: setKey, toString, deserialize, serialize, createInstance */
	public Draft setKey() {
	    this.key = State.makeKey(new String[] { 
		    String.valueOf(this.year),
		    String.valueOf(this.round),
		    originalTeam
		});
	    return this;
    }

    @Override
    public String toString() {
        return "Draft::" + this.key + " owned by " + this.ownerTeam;
    }

    /**
     * Deserialize a state data to commercial paper
     *
     * @param {Buffer} data to form back into the object
     */
    public static Draft deserialize(byte[] data) {
		JSONObject json;
		try{
        	json = new JSONObject(new String(data, UTF_8));
		} catch (org.json.JSONException exc){
			return null;
		}
        int year = json.getInt("year");
        int round = json.getInt("round");
        String originalTeam = json.getString("originalTeam");
        String ownerTeam = json.getString("ownerTeam");
        
        return createInstance(year, round, originalTeam, ownerTeam);
    }

    public static byte[] serialize(Draft draft) {
        return State.serialize(draft);
    }

    /**
     * Factory method to create a commercial paper object
     */
    public static Draft createInstance(int year, int round, String originalTeam,
    		String ownerTeam) {
        return new Draft().setYear(year).setRound(round).setOriginalTeam(originalTeam)
        		.setOwnerTeam(ownerTeam).setKey();
    }

	public static boolean isValidRound(int round){
		return round >= 1 && round <= 3;
	}

	@Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        Draft other = (Draft) obj;

        return Objects.deepEquals(
            new String[] {getOriginalTeam(), getOwnerTeam()},
            new String[] {other.getOriginalTeam(), other.getOwnerTeam()}
        ) && getYear() == other.getYear() && getRound() == other.getRound();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getYear(), getRound(), getOriginalTeam(), getOwnerTeam());
    }
}
