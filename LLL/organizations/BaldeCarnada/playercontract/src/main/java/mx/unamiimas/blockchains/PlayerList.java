/*
SPDX-License-Identifier: Apache-2.0
*/

package mx.unamiimas.blockchains;

import mx.unamiimas.blockchains.ledgerapi.StateList;
import org.hyperledger.fabric.contract.Context;
import org.hyperledger.fabric.shim.ledger.CompositeKey;

public class PlayerList {

    private StateList stateList;

    public PlayerList(Context ctx) {
        this.stateList = StateList.getStateList(ctx, PlayerList.class.getSimpleName(), Player::deserialize);
    }

    public PlayerList addPlayer(Player player) {
        stateList.addState(player);
        return this;
    }

    public Player getPlayer(String playerKey) {
        return (Player) this.stateList.getState(playerKey);
    }

    public PlayerList updatePlayer(Player player) {
        this.stateList.updateState(player);
        return this;
    }

    public CompositeKey getPartialCompositeKey(){
        return this.stateList.getPartialCompositeKey();
    }
}
