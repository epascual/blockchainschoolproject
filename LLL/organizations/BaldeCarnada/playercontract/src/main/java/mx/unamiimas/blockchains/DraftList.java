package mx.unamiimas.blockchains;

import org.hyperledger.fabric.contract.Context;

import mx.unamiimas.blockchains.ledgerapi.StateList;
import org.hyperledger.fabric.shim.ledger.CompositeKey;

public class DraftList {
	
	private StateList stateList;
	
	public DraftList(Context ctx) {
	    this.stateList = StateList.getStateList(ctx, DraftList.class.getSimpleName(), Draft::deserialize);
    }

    public DraftList addDraft(Draft draft) {
        stateList.addState(draft);
        return this;
    }
    
    public Draft getDraft(String draftKey) {
        return (Draft) this.stateList.getState(draftKey);
    }

    public DraftList updateDraft(Draft draft) {
        this.stateList.updateState(draft);
        return this;
    }
    
    //OBSOLETO
    //metodo member para verificar si un elemento draft pertenece a una DraftList
    public boolean memberDraftList(Draft draft) {
    	return false;
    }

    public CompositeKey getPartialCompositeKey(){
        return this.stateList.getPartialCompositeKey();
    }
}
