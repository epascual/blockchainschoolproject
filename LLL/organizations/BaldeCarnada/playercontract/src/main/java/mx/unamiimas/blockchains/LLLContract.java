/*
  SPDX-License-Identifier: Apache-2.0
*/
package mx.unamiimas.blockchains;

import java.util.logging.Logger;

import mx.unamiimas.blockchains.ledgerapi.State;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.hyperledger.fabric.contract.Context;
import org.hyperledger.fabric.contract.ClientIdentity;

import org.hyperledger.fabric.contract.ContractInterface;
import org.hyperledger.fabric.contract.annotation.Contact;
import org.hyperledger.fabric.contract.annotation.Contract;
import org.hyperledger.fabric.contract.annotation.Default;
import org.hyperledger.fabric.contract.annotation.Info;
import org.hyperledger.fabric.contract.annotation.License;
import org.hyperledger.fabric.contract.annotation.Transaction;
import org.hyperledger.fabric.shim.ChaincodeException;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ledger.KeyValue;
import org.hyperledger.fabric.shim.ledger.QueryResultsIterator;
import org.hyperledger.fabric.shim.ledger.CompositeKey;

import org.json.JSONException;

@Contract(name = "mx.unamiimas.blockchains.player", info = @Info(title = "MyAsset contract", description = "", version = "0.0.1", license = @License(name = "SPDX-License-Identifier: ", url = ""), contact = @Contact(email = "java-contract@example.com", name = "java-contract", url = "http://java-contract.me")))
@Default
public class LLLContract implements ContractInterface {

	// use the classname for the logger, this way you can refactor
	private final static Logger LOG = Logger.getLogger(LLLContract.class.getName());

	@Override
	public Context createContext(ChaincodeStub stub) {
		return new LLLContext(stub);
	}

	public LLLContract() {

	}

	/**
	* Instantiate to perform any setup of the ledger that might be required.
	*
	* @param {Context} ctx the transaction context
	*/
	@Transaction
	public void instantiate(LLLContext ctx) {
		LOG.info("No data migration to perform");
	}
    
	/**
	* Transaccion que contrata un jugador con sus datos correspondientes a un equipo. Si el jugador no existe 
	* previamente en la liga, se crea y se agrega a DraftList, si ya existe, se actualizan sus valores
	* siempre y cuando sea agente libre.
	* @param{Context} ctx contexto del ledger
	* @param{String} curm cadena para identificar al jugador a contratar
	* @param{String} name cadena con el nombre del jugador
	* @param{String} newowner cadena para identificar al equipo al que se contrata al jugador
	* @param{String} newcontract cadena que contiene las especificaciones del contrato del jugador
	* @param{Integer} newlastyearhired anio de termino del nuevo contrato para el jugador
	* @param{Integer} thisyear OBSOLETO valor del anio actual con el que se empezara el contrato
	* @return{Player} Regresa el jugador nuevo agregado si el jugador no estaba en la liga, regresa el jugador si 
	* ya existia en la liga y regresa <code>null</code> si algún error ocurre
	*/
	@Transaction
	public UpdateResult hirePlayer(LLLContext ctx, String curm,  String name, String newowner,
									String newcontract , int newlastyearhired){

		String transactionSubmitter = ctx.getClientIdentity().getMSPID();
		System.out.println(transactionSubmitter);

		//Verificando que quien hace la transaccion pertenece al equipo newowner
		if (!transactionSubmitter.equals(getMSPIDofTeam(newowner))){
			return new UpdateResult(false, "Identidad no verificada");
		}
		
		final int thisyear = getCurrentYear();
		String playerKey = State.makeKey(new String[] {curm});
		Player player = ctx.playerList.getPlayer(playerKey);
		//Caso en que es un jugador nuevo en la liga
		if(player==null) {
			if(newlastyearhired<thisyear) {
				return new UpdateResult(false, "El contrato del nuevo jugador tiene fecha de vencimiento invalida");
			}
			Player newPlayer = Player.createInstance(curm, name, newowner, newcontract, thisyear, newlastyearhired);
			System.out.println("Created player " + newPlayer.toString());
			ctx.playerList.addPlayer(newPlayer);
			return new UpdateResult(true, "Nuevo jugador con CURM " + curm + " contratado exitosamente");
		}
		//	Caso en que el jugador ya existia en la liga, es decir, el jugador es debe de ser agente libre
		// Validacion de que es agente libre 
		if(!player.esAgenteLibre(thisyear)) {
			System.err.println("The player:" + player.toString() + " is not a free agent.");
			return new UpdateResult(false, "Un jugador con CURM " + curm + " esta actualmente contratado");
		}
		//Actualizamos los valores en player y actualizamos la lista en playerList
		player.setOwner(newowner);
		player.setContract(newcontract);
		player.setYearhired(thisyear);
		player.setLastyearhired(newlastyearhired);
		ctx.playerList.updatePlayer(player);
		
		System.out.println("Rehired player " + player.toString());
		return new UpdateResult(true, "Agente libre con CURM " + curm + " contratado exitosamente");
	}
    
	/**
	* Transaccion para despedir a un jugador. Establece lastyearhired como el anio actual-1 para asegurar que sea un agente libre
	* @param{Context} ctx contexto del ledger
	* @param{String} curm cadena para identificar al jugador a ser despedido
	* @param{String} currentowner cadena para identificar al equipo al que labora el jugador a ser despedido
	* @param{Integer} thisyear OBSOLETO anio actual para verificar la vigencia del contrato del jugador actual
	* @return{UpdateResult} regresa la tupla UpdateResult con el valor <code>true</code> si la transacción fue exitosa
	* junto con el mensaje correspondiente al usuario. Regresa <code>false</code> si hubo algún problema en la validacion
	* junto con un mensaje al usuario indicando donde fue el problema de la validacion
	*/
	@Transaction
	public UpdateResult firePlayer(LLLContext ctx, String curm, String currentowner) {

		String transactionSubmitter = ctx.getClientIdentity().getMSPID();
		System.out.println(transactionSubmitter);

		//Verificando que quien hace la transaccion pertenece al equipo currentowner
		if (!transactionSubmitter.equals(getMSPIDofTeam(currentowner))){
			return new UpdateResult(false, "Identidad no verificada");
		}
		
		String playerKey = State.makeKey(new String[] { curm });
		Player player = ctx.playerList.getPlayer(playerKey); 
		final int thisyear = getCurrentYear();
		//Validar que el Jugador ya exista en la liga
		if(player == null) {
			System.err.println("The player with curm: " + curm + " -is not a part of the league.");
			return new UpdateResult(false, "El jugador con CURM " + curm + " no es parte de la liga");
		}
		
		//Validar que el contrato esta vigente
		if(player.esAgenteLibre(thisyear)) {
			System.err.println("The player:" + player.toString() + " is a free agent.");
			return new UpdateResult(false, "El jugador con CURM " + curm + " no esta actualmente contratado");
		}
		//Validar que el duenio sea el correcto esta vigente
		if(!player.getOwner().equals(currentowner)) {
			System.err.println("The player:" + player.toString() + " is not owned by the team "+currentowner+".");
			return new UpdateResult(false, "El jugador con CURM " + curm + " no esta actualmente contratado por su equipo");
		}
		//Se actualiza el valor de lastyearhired para que el contrato este caduco y se actualiza PlayerList
		player.setLastyearhired(thisyear-1);
		player.setOwner("Agente Libre");
		ctx.playerList.updatePlayer(player); 
		
		System.out.println("Fired player "+player.toString());
		return new UpdateResult(true, "Jugador con CURM " + curm + " dado de baja del equipo exitosamente");
	}
    
	/**
	* Busca un jugador en la base de datos a traves de su curm
	* @param{Context} ctx contexto del ledger
	* @param{String} curm valor para identificar de forma unica a un jugador 
	* @return{Player} player jugador buscado con los valores anteriores, null si no existe
	*/
	@Transaction
	public Player queryPlayer(final LLLContext ctx, final String curm) {
		String playerKey = State.makeKey(new String[] { curm });
		Player player = ctx.playerList.getPlayer(playerKey);        

		if (player != null){
			System.out.println("Returned player: " + player.toString());
		} else {
			System.out.println("Player not found, curm is: " + curm);
		}

		return player;
	}


	/**
	* Busca un draft en la base de datos con base en el anio de draft, ronda y equipo original de donde proviene 
	* @param{Context} ctx contexto del ledger
	* @param{Integer} year
	* @param{Integer} round
	* @param{String} orignialteam
	* @return{Draft} draft el draft buscado con los valores, null si no existe
	*/
	@Transaction
	public Draft queryDraft(final LLLContext ctx, final int year,final int round, final String originalteam) {
		
		String draftKey = State.makeKey(new String[] { String.valueOf(year),String.valueOf(round),originalteam });
		Draft draft = ctx.draftList.getDraft(draftKey);        

		if (!Draft.isValidRound(round)){
			System.out.println("Draft quiered has invalid round: " + round);
			return null;
		}

		//Si el draft ya existe en el ledger
		if (draft != null){
			System.out.println("Returned Draft: " + draft.toString());
			return draft;
		} 

		System.out.println("Draft not found, draft key is: " + draftKey);

		//Creando draft por defecto
		draft = Draft.createInstance(year, round, originalteam, originalteam);
		return draft;
	}

	
	/**
	* Regresa arreglo de todos los jugadores                                                                              
	*                                                                                                                                        
	* @param ctx El contexto                                                                                                                     
	* @return Arreglo de todos los jugadores.                                                                                                            
	*/
	@Transaction()
	public Player[] queryAllPlayers(final LLLContext ctx) {

		ChaincodeStub stub = ctx.getStub();

		List<Player> queryResults = new ArrayList<Player>();
		CompositeKey prefix = ctx.playerList.getPartialCompositeKey();
		System.out.println("Buscando valores para " + prefix);

		// Iteramos sobre todas las llaves de jugadores.
		QueryResultsIterator<KeyValue> results = stub.getStateByPartialCompositeKey(prefix);

		for (KeyValue result: results) {
			System.out.println("Deserializing: " + result.getStringValue());

			Player player = Player.deserialize(result.getValue());
			if (player != null){	
				queryResults.add(player);
			}
		}

		Player[] response = queryResults.toArray(new Player[queryResults.size()]);
		System.out.println("QueryAllPlayers devolviendo " + response.length + " jugadores");

		return response;
	}
    
	/**
	* Regresa arreglo de todos los drafts                                   
	* @param ctx El contexto                             
	* @return Arreglo de todos los drafts.                                 
	*/
	@Transaction()
	public Draft[] queryAllDrafts(final LLLContext ctx) {

		ChaincodeStub stub = ctx.getStub();

		// Resultados de la búsqueda.
		List<Draft> queryResults = new ArrayList<Draft>();

		// Llave compuesta para buscar.
		CompositeKey prefix = ctx.draftList.getPartialCompositeKey();
		System.out.println("Buscando valores para " + prefix);

		// Iteramos sobre todas las llaves de draft.
		QueryResultsIterator<KeyValue> results = stub.getStateByPartialCompositeKey(prefix);

		for (KeyValue result: results) {
		System.out.println("Deserializing: " + result.getStringValue());

		// Draft actual
		Draft draft = Draft.deserialize(result.getValue());
			if (draft != null){	
				queryResults.add(draft);
			}
		}

		Draft[] response = queryResults.toArray(new Draft[queryResults.size()]);
		return response;
	}
    
	/**
	* Regresa arreglo de todos los drafts que pertenecen al equipo gracias a un intercambio.                                                            
	* @param ctx El contexto                                     
	* @param equipo El equipo sobre el que haremos la consulta.
	* @return Arreglo de todos los drafts obtenidos a través de intercambios     */
	@Transaction()
	public Draft[] queryObtainedDrafts(final LLLContext ctx, final String equipo) {
		// Lista de drafts obtenidos en intercambios.
		ArrayList<Draft> obtenidos = new ArrayList<>();
		for(Draft actual: queryAllDrafts(ctx)){
			if(actual.getOwnerTeam().equals(equipo) && !actual.getOriginalTeam().equals(equipo))
				obtenidos.add(actual);
		}
		// Draft a regresar
		Draft[] resultado = new Draft[obtenidos.size()];
		return obtenidos.toArray(resultado);	    
	}


	/**
	* Regresa arreglo de todos los drafts que pertenecían al equipo pero ya no.                                                            
	* @param ctx El contexto                                     
	* @param equipo El equipo sobre el que haremos la consulta.
	* @return Arreglo de todos los drafts intercambiados
	*/
	@Transaction()
	public Draft[] queryTradedDrafts(final LLLContext ctx, final String equipo) {
		// Lista de drafts intercambiados
		ArrayList<Draft> intercambiados = new ArrayList<>();
		for(Draft actual: queryAllDrafts(ctx)){
			// Draft actual del recorrido.
			if(!actual.getOwnerTeam().equals(equipo) && actual.getOriginalTeam().equals(equipo))
				intercambiados.add(actual);
		}
		// Draft a regresar
		Draft[] resultado = new Draft[intercambiados.size()];
		return intercambiados.toArray(resultado);	    
	}
    
	/**
	* Regresa un arreglo de jugadores agentes libres. 
	* @param ctx El contexto del ledger.
	* @param thisyear OBSOLETO Anio actual.
	* @return Arreglo con los jugadores que son agentes libres.
	*/
	@Transaction
	public Player[] queryFreeAgentPlayers(final LLLContext ctx) {
		final int thisyear = getCurrentYear();
		// Lista de agentes libres.
		ArrayList<Player> agentesLibres = new ArrayList<>();
		for(Player jugador : queryAllPlayers(ctx)){
			if(jugador.esAgenteLibre(thisyear))
				agentesLibres.add(jugador);
		}
		// Jugadores a regresar.
		Player[] jugadores = new Player[agentesLibres.size()];
		return agentesLibres.toArray(jugadores);
	}

	/**
	* Regresa un arreglo de jugadores pertenecientes al equipo dado. 
	* @param ctx El contexto del ledger.
	* @return Arreglo con los jugadores que son agentes libres.
	*/
	@Transaction
	public Player[] queryTeamPlayers(final LLLContext ctx, final String team) {
		final int thisyear = getCurrentYear();
		// Lista de jugadores pertenecientes al equipo.
		ArrayList<Player> teamPlayers = new ArrayList<>();
		
		for(Player jugador : queryAllPlayers(ctx)){
			if(jugador.getOwner().equals(team) && !jugador.esAgenteLibre(thisyear))
				teamPlayers.add(jugador);
		}
		// Jugadores a regresar.
		Player[] jugadores = new Player[teamPlayers.size()];
		return teamPlayers.toArray(jugadores);
	}
     	   
	/**
	* Version alternativa de trade que recibe arreglos de cadenas
	* Para jugadores solo se tiene un arreglo con los curm's de los jugadores correspondientes
	* Ejemplo: [curm1,curm2,...,curm_n]
	* Para los drafts se tiene un arreglo de arreglos de tamanio 3 (de tipo String). Cada arreglo de tamanio 3 tiene los datos para 
	* identificar un draft, esto es year, round y originalTeam. Ejemplo [[year1,round1,originalteam1],[year2,round2,originalteam2]]
	* 
	* Metodo de intercambio de conjunto de jugadores y conjunto de drafts por conjunto de jugadores y conjunto de drafts
	* Recibe el anio actual para poder verificar si los jugadores son agentes libres 
	* Intercambio de assets (jugadores o drafts).
	* @param{Context} ctx contecto del ledger
	* @param{String[]} lstjugadores1 lista de curm's de jugadores a intercambiar del equipo 1 al equipo 2
	* @param{String[]} lstjugadores2 lista de curm's de jugadores a intercambiar del equipo 2 al equipo 1
	* @param{String[][]} lstdrafts1 lista de arreglos de longitud 3 que representan selecciones de draft a intercambiar del equipo 1 al equipo 2
	* @param{String[][]} lstdrafts2 lista de arreglos de longitud 3 que representan selecciones de draft a intercambiar del equipo 2 al equipo 1
	* @param{String} equipo1  
	* @param{String} equipo2
	* @return{UpdateResult} regresa la tupla UpdateResult con el valor <code>true</code> si la transacción fue exitosa
	* junto con el mensaje correspondiente al usuario. Regresa <code>false</code> si hubo algún problema en la validacion
	* junto con un mensaje al usuario indicando donde fue el problema de la validacion
	*/
	@Transaction
	public UpdateResult trade(LLLContext ctx, String[] lstjugadores1, String[][] lstdrafts1, 
					String[] lstjugadores2, String[][] lstdrafts2, String equipo1, String equipo2){
		
		String transactionSubmitter = ctx.getClientIdentity().getMSPID();
		System.out.println(transactionSubmitter);

		//Verificando que quien hace la transaccion pertenece al equipo equipo1
		if (!transactionSubmitter.equals(getMSPIDofTeam(equipo1))){
			return new UpdateResult(false, "Identidad no verificada");
		}

		int thisyear = getCurrentYear();
			
		// Primero se validan todos que todos los assets se puedan transferir
		Set<String> setjugadores1 = new HashSet<>(Arrays.asList(lstjugadores1));
		Set<String> setjugadores2 = new HashSet<>(Arrays.asList(lstjugadores2));
		Set<String[]> setdrafts1 = new HashSet<>(Arrays.asList(lstdrafts1));
		Set<String[]> setdrafts2 = new HashSet<>(Arrays.asList(lstdrafts2));

		for (int validateAt0=0; validateAt0 < 2; validateAt0++){
			boolean justValidate = validateAt0 == 0;
			// Primero hay que verificar para cada jugador que pertenezca al equipo correspondiente.
			for(String curm: setjugadores1){
				UpdateResult result = exchangePlayer(ctx,curm,equipo1,equipo2, justValidate);
				if(!result.getResult()) {
					return result;
				}
			}

			for(String curm: setjugadores2){
				UpdateResult result = exchangePlayer(ctx,curm,equipo2,equipo1, justValidate);
				if(!result.getResult()) {
					return result;
				}	
			}

			// Lo mismo para drafts.
			for(String[] draftvalues:setdrafts1){
				UpdateResult result = exchangeDraft(ctx, Integer.parseInt(draftvalues[0]), Integer.parseInt(draftvalues[1]), 
									draftvalues[2], equipo1, equipo2, justValidate);
				if (!result.getResult()){
					return result;
				}	
			}

			for(String[] draftvalues:setdrafts2){
				UpdateResult result = exchangeDraft(ctx, Integer.parseInt(draftvalues[0]), Integer.parseInt(draftvalues[1]), 
									draftvalues[2], equipo2, equipo1, justValidate);
								
				if (!result.getResult()){
					return result;
				}
			}
		}
		// AQUI SE HACEN LAS ACTUALIZACIONES AL LEDGER, DESPUES DE VALIDARSE

		return new UpdateResult(true, "Intercambio realizado exitosamente");
	}
  
    
	/**
	* Transaccion unitaria de un jugador prexistente en la liga que no sea agente libre de un equipo a otro 
	* requiere el anio actual para un correcto funcionamiento
	* @param{Context} ctx Contexto
	* @param{String} curm identificador del jugador a intercambiar
	* @param{String} currentowner nombre del equipo actual donde labora el jugador actual
	* @param{String} newowner nombre del nuevo equipo donde sera transferido el jugador actal 
	* @param{Integer} thisyear OBSOLETO anio actual para poder determinar si el jugador es agente libre o no
	* @return{UpdateResult} regresa la tupla UpdateResult con el valor <code>true</code> si la transacción fue exitosa
	* junto con el mensaje correspondiente al usuario. Regresa <code>false</code> si hubo algún problema en la validacion
	* junto con un mensaje al usuario indicando donde fue el problema de la validacion
	*/
	@Transaction
	public UpdateResult exchangePlayer(LLLContext ctx, String curm,String currentowner,String newowner, boolean justValidate) {
		String playerKey = State.makeKey(new String[] {curm});
		Player player = ctx.playerList.getPlayer(playerKey);
		int thisyear = getCurrentYear();
		//Validar que el Jugador ya exista en la liga
		if(player == null) {
			System.err.println("The player with curm: " + curm + " -is not a part of the league.");
			return new UpdateResult(false, "El jugador con CURM " + curm + " no es parte de la liga");
		}
		
		//Validar que el contrato esta vigente
		if(player.esAgenteLibre(thisyear)) {
			System.err.println("The player:" + player.toString() + " is a free agent.");
			return new UpdateResult(false, "El jugador con CURM " + curm + " no esta actualmente contratado en la liga");
		}
		
		//Validar que el duenio sea el correcto esta vigente
		if(!player.getOwner().equals(currentowner)) {
			System.err.println("The player:" + player.toString() + " is not owned by the team "+currentowner+".");
			return new UpdateResult(false, "El jugador con CURM " + curm + " no esta actualmente contratado por el equipo " + currentowner);
		}
		
		if (!justValidate){
			player.setOwner(newowner);
			ctx.playerList.updatePlayer(player); 
		}

		System.out.println("Player traded "+player.toString());
		return new UpdateResult(true, "Jugardo con CURM " + curm + " transferido exitosamente");
	}


	/**
		* Transaccion unitaria de seleccion de draft entre dos equipos
		* @param{Context} ctx contexto 
		* @param{Integer} year anio de la seleccion de draft a intercambiar
		* @param{Integer} round ronda de la seleccion de draft a intercambiar
		* @param{String} originalteam  nombre del equipo original de la seleccion de draft a intercambiar
		* @param{String} currentowner nombre del equipo actual de la seleccion de draft a intercambiar
		* @param{String} newowner nombre del equipo al que se le transferira la propiedad de la seleccion de draft a intercambiar
		* @return{Draft} draft nuevo si no se había intercambiado antes, draft con valor de owner actualizado si ya se había intercambiado antes
		* null si algo salio mal
		*/
	@Transaction
	public UpdateResult exchangeDraft(LLLContext ctx, int year,int round,String originalteam,String currentowner,String newowner, boolean justValidate) {
		String draftKey = State.makeKey(new String[] { String.valueOf(year),String.valueOf(round),originalteam});
		Draft draft = ctx.draftList.getDraft(draftKey); 

		// Anio actual
		int thisyear = getCurrentYear();
		if(thisyear > year){
			// No se puede intercambiar un draft vencido.
			System.err.println("No puedes intercambiar drafts de años anteriores");
			return new UpdateResult(false, "La seleccion de draft del " + year +", ronda "+ round + " y equipo " + 
						originalteam + " es de un año que ya pasó");
		}

		//Validar para el caso de que el draft no haya sido intercambiado antes
		if(draft == null) {
			if(!originalteam.contentEquals(currentowner)) {
				System.err.println("The draft pick of the year " + year +", round "+ round+ " and original owner "+ 
							originalteam + "is not owned by the team "+ currentowner);
				return new UpdateResult(false, "La seleccion de draft del " + year +", ronda "+ round + " y equipo " + 
							originalteam + " no le pertenece actualmente al equipo "+ currentowner);
			} else {
				draft = Draft.createInstance(year,round,originalteam,newowner);
				if (!justValidate){
					ctx.draftList.addDraft(draft);
				}
				System.out.println("Draft traded "+draft.toString());
							
				return new UpdateResult(true, "Draft " + draft.toString() + " transferido");
			}
		}
		//Validacion para el caso en que el draft ya haya sido intercambiado antes
		if(!draft.getOwnerTeam().equals(currentowner)) {
			System.err.println("The draft pick of the year " + year +", round "+ round+ " and original owner "+ 
						originalteam + "is not owned by the team "+ currentowner);
			return new UpdateResult(false, "La seleccion de draft del " + year + ", ronda "+ round + " y equipo " + 
						originalteam + " no le pertenece actualmente al equipo "+ currentowner);
		} else {
			if (!justValidate){
				draft.setOwnerTeam(newowner);
				ctx.draftList.updateDraft(draft);
			}
			System.out.println("Draft traded "+draft.toString());
				
			return new UpdateResult(true, "Draft " + draft.toString() + " transferido");
		}
	}

	/**
	 * Metodo privado para representar el anio actual
	 * @return{Integer} el anio actual
	 */
	private static int getCurrentYear() {
		return 2020;
	}	

	private static String getMSPIDofTeam(String team){
		if (team.equals("Balde de Carnada"))
			return "BaldeCarnadaMSP";
		if (team.equals("Crustaceo Cascarudo"))
			return "CrustaceoCascarudoMSP";
		if (team.equals("Osos Marinos"))
			return "OsosMarinosMSP";
		return "ErrorInvalidMSP";
	} 
}
